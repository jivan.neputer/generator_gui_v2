<?php


namespace App\Foundation\Concerns;

use Illuminate\Support\Str;
use File;
use Image;

trait InterventionImageTrait
{
    /**
     * $file_filed_name is a image input name it should be file
     * if you want use different name just change the value of $file_filed_name with
     * your desire name
     *
     * @var string
     */
    protected $file_filed_name = 'file';

    /**
     * Get Folder name For child image
     *
     * @return string
     */
    public function folderName(){
        $model = $this->model->getTable();
        return Str::singular($model);
    }

    /**
     * Root path for image
     *
     * @return string
     */
    public function rootPath(){
        return public_path(DIRECTORY_SEPARATOR.'images');
    }

    /**
     * child path for image
     *
     * @return string
     */
    public function childPath(){
        return public_path(DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folderName());
    }

    /**
     * Get random number for image
     *
     * @return string
     */
    public function getRandomString(){
        return $this->folderName().'-'.time().mt_rand(4100, 99999);
    }

    /**
     * Make folder for root and child
     *
     */
    public function makeFolder(){
        // parent folder
        if (!file_exists($this->rootPath())) {
            File::makeDirectory($this->rootPath(), 0775, true, false);
        }
        // child folder
        if (!file_exists($this->childPath())) {
            File::makeDirectory($this->childPath(), 0775, true, false);
        }
    }

    /**
     * Move file into child Folder
     *
     * @param $img
     * @param $nameWithExt
     */
    public function moveImage($img,$nameWithExt){
        if(!$img->move($this->childPath(), $nameWithExt))
        {
            dd('issue in file uploading');
        }
    }

    /**
     * @param $imageName
     */
    public function thumbsImage($imageName){
        $imageDimensions = config('image_dimensions.'.$this->folderName());
        if(isset($imageDimensions)){
            foreach ($imageDimensions as $dimension) {
                $img = Image::make($this->childPath(). DIRECTORY_SEPARATOR . $imageName)
                    ->resize($dimension['width'], $dimension['height'], function ($constraint) {
                        $constraint->aspectRatio();
                    });
                $img->save($this->childPath() . DIRECTORY_SEPARATOR . $dimension['width'] . '_' . $dimension['height'] . '_' . $imageName, $dimension['quality']);
            }
        }
    }

    /**
     * remove thumbnails image
     *
     * @param $dataImageName
     */
    public function removeThumbsImage($dataImageName)
    {
        $imagePath = $this->childPath().DIRECTORY_SEPARATOR;
        $imageDimensions = config('image_dimensions.'.$this->folderName());
        if(isset($imageDimensions)){
            foreach ($imageDimensions as $dimension) {
                $thumbImages = $imagePath.$dimension['width'].'_'.$dimension['height'].'_'.$dataImageName;
                if(file_exists($thumbImages)){
                    @unlink($thumbImages);
                }
            }
        }
    }
    /**
     * Remove image
     *
     * @param $dataImageName
     */
    public function removeImage($dataImageName){
        $imagePath = $this->childPath().DIRECTORY_SEPARATOR.$dataImageName;
        if(file_exists($imagePath)){
            @unlink($imagePath);
        }
        $this->removeThumbsImage($dataImageName);
    }

    /**
     * process image
     *
     * @param $request
     * @return string
     */
    public function processImage($request, $imageName){
        $img = $request->file($imageName);
        $imgExtension = explode('.',$img->getClientOriginalName());
        $extension = last($imgExtension);
        $nameWithExt=$this->getRandomString() . '.' .$extension;

        $this->makeFolder();
        $this->moveImage($img,$nameWithExt);
        $this->thumbsImage($nameWithExt);

        return $nameWithExt;
    }

    /**
         * upload image
     *
     * @param $request
     * @return string
     */
    public function uploadImage($request,$imageName){
        if($request->hasFile($imageName)) {
            return $this->processImage($request, $imageName);
        }

    }

    /**
     * update image
     *
     * @param $request
     * @param $dataImageName
     * @return string
     */
    public function updateImage($request, $dataImageName, $imageName){
        if($request->hasFile($imageName)) {
            $this->removeImage($dataImageName);
            return $this->processImage($request, $imageName);
        }
    }
}
