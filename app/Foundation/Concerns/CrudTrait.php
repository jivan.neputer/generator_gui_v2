<?php

namespace App\Foundation\Concerns;

/**
 * Trait CrudTrait
 * @package Neputer\Supports\Concerns
 */
trait CrudTrait
{

    /**
     * @param $id
     * @return mixed
     */
    public function findOrFail($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param array $data
     * @param array $search
     * @return mixed
     */
    public function createOrUpdate( array $data, array $search)
    {
        return $this->model->updateOrCreate(
            $search,
            $data
        );
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function new( array $data )
    {
        return $this->model->create( $data );
    }

    /**
     * @param array $data
     * @param $model
     * @return mixed
     */
    public function update( array $data, $model)
    {
        $instance = $model;
        $model->update( $data );
        return $instance;
    }

    /**
     * @param null $paginate
     * @return mixed
     */
    public function get( $paginate = null )
    {
        $that = $this->model;

        if (is_null($paginate)) {
            return $that->latest()
                ->get();
        }
        return $that->latest()->paginate($paginate);
    }

    /**
     * Here $data is new created record of that model
     * And $tags is single or multiple tag value
     *
     * @param $data
     * @param $tags
     * @return mixed
     */
    public function syncData($data,$tags)
    {
        return $data->tags()->sync((array) $tags);
    }

    /**
     * @param $model
     * @return bool
     */
    public function delete($model)
    {
        $model->delete();
        return true;
    }

    /**
     * @param $id
     */
    public function deleteChild($model,$id)
    {
        $model::where('id',$id)->delete();
        return true;
    }

    /**
     * Return the instance of the current model
     *
     * @return mixed
     */
    public function query()
    {
        return $this->model->query();
    }

    /**
     * Get desire column
     * @param $id
     * @param $column
     * @return mixed
     */
    public function getColumnById($id, $column){
        return $this->model
            ->select($column)
            ->where('id',$id)
            ->first();
    }

}
