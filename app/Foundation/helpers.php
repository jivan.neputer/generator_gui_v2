<?php
if (! function_exists('is_active') ) :

    /**
     * Check if given route is active
     *
     * @return boolean
     */
    function is_active(string $route)
    {
        return request()->route()->getName() === $route;
    }
endif;
if (! function_exists('is_json')) :

    function is_json($string){
        return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }

endif;

//if (! function_exists('flash') ) :
//
//    function flash($type, $message)
//    {
////        return app(\Neputer\Lib\Flash::class)->notify($type, $message);
//        <script>
//        if ($type == 'success'):
//            setTimeout(function() {
//                toastr.options = {
//                    closeButton: true,
//                        progressBar: true,
//                        showMethod: 'slideDown',
//                        timeOut: 4000
//                    };
//                    toastr.success("{{ Session::get('success') }}");
//
//                }, 1300);
//            endif;
//        </script>
//
//    }
//endif;
