<?php

namespace App\Foundation\Base;



use App\Foundation\Concerns\CrudTrait;

/**
 * Class BaseService
 * @package Neputer\Supports
 */
abstract class BaseService
{

    use CrudTrait;

}
