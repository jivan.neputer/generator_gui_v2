<?php

namespace App\Foundation\Base;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;

/**
 * Class BaseController
 *
 * @package Neputer\Supports
 */
abstract class BaseController extends Controller
{


    protected $pagination_limit = 10;

    /**
     * Redirect if save & continue
     *
     * @param Request $request
     * @return RedirectResponse
     */
    protected function redirect(Request $request)
    {
        if ($request->has('submit_continue')) {
            return back();
        }
        return redirect()->route( pathinfo($request->route()->getName(), PATHINFO_FILENAME).'.index');
    }

    public function pagination_limit()
    {
        return 10;
    }

}
