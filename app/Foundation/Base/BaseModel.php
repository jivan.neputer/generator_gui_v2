<?php

namespace App\Foundation\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseModel
 * @package App\Generator\Base
 */
abstract class BaseModel extends Model
{

}
