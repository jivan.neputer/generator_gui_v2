<?php

namespace App\Models;

use App\Foundation\Base\BaseModel;

/**
 * Class Product
 * @package App\Models
 */
class Product extends BaseModel
{
    protected $fillable = [
            'name', 'image', 'number', 'status'
    ];
}
