<?php

namespace App\Http\Controllers\Admin\Generator;

use App\Generator\Supports\Generator\Handlers\ControllerHandler;
use App\Generator\Supports\Generator\Handlers\FillableHandler;
use App\Generator\Supports\Generator\Handlers\MenuHandler;
use App\Generator\Supports\Generator\Handlers\MigrationHandler;
use App\Generator\Supports\Generator\Handlers\ModelPermissionHandler;
use App\Generator\Supports\Generator\Handlers\RequestHandler;
use App\Generator\Supports\Generator\Handlers\RouteHandler;
use App\Generator\Supports\Generator\Handlers\seederFactoryHandler;
use App\Generator\Supports\Generator\Handlers\seederHandler;
use App\Generator\Supports\Generator\Handlers\ServiceHandler;
use App\Generator\Supports\Generator\Handlers\ViewHandler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class GeneratorController extends Controller
{
    /**
     * @var string
     */
    public $migration = 'migration';


    public function __construct()
    {

    }

    public function create(Request $request)
    {
        try{
            $this->initGenerator($request);
            exec('composer dump-autoload');
//            shell_exec('composer dump-autoload');
//            \Artisan::call('dump-autoload');
            return redirect()->back();
        }
        catch (\Exception $ex){
            dd($ex);
        }
    }

    /**
     * @param $request
     */
    public function initGenerator($request)
    {
        $permittedModelList = ModelPermissionHandler::handle($request);
        self::checkExecuteModelPermission($request, $permittedModelList);
    }


    /**
     * @param $request
     */
    public function migration($request)
    {
        MigrationHandler::getMigration($request->get('modelName'),$request->get('migration'));
        $request->session()->flash('alert-success', 'Generator has successfully generated the Migration for you!');
    }

    /**
     * @param $request
     */
    public function model($request)
    {
        FillableHandler::getFillable($request->get('modelName'),$request->get('modelFillable'));
        $request->session()->flash('alert-success', 'Generator has successfully generated the Model for you!');
    }

    /**
     * @param $request
     */
    protected function request($request)
    {
        RequestHandler::getRequest($request->get('modelName'));
        $request->session()->flash('alert-success', 'Generator has successfully generated the Request for you!');
    }

    /**
     * @param $request
     */
    protected function seederAndFactories($request)
    {
        SeederFactoryHandler::getSeederFactory($request->get('modelName'), $request->get('seederFactory'));
        $request->session()->flash('alert-success', 'Generator has successfully generated Seeder and Factor for you!');
    }

    /**
     * @param $request
     */
    protected function appendSeeder($request){
        seederHandler::appendSeeder($request->get('modelName'));
    }

    /**
     * @param $request
     */
    protected function routes($request){
        RouteHandler::getRoutes($request->get('modelName'));
        $request->session()->flash('alert-success', 'Generator has successfully generated the Route for you!');
    }

    /**
     * @param $request
     */
    protected function service($request){
        ServiceHandler::getService($request);
        $request->session()->flash('alert-success', 'Generator has successfully generated the Service for you!');
    }

    /**
     * @param $request
     */
    protected function controller($request){
        ControllerHandler::getController($request);
        $request->session()->flash('alert-success', 'Generator has successfully generated the Controller for you!');
    }

    protected function appendMenu($request){
        MenuHandler::handle($request->get('modelName'));
        $request->session()->flash('alert-success', 'Generator has successfully appended menu for you!');
    }

    protected function views($request){
        ViewHandler::handle($request);
        $request->session()->flash('alert-success', 'Generator has successfully generated view for you!');
    }




    /**
     * @param $request
     * @param $permittedModelList
     */
    public function checkExecuteModelPermission($request, $permittedModelList)
    {
        foreach($permittedModelList as $key => $modelList) {
            if($modelList == 'migration'){
                $this->migration($request);
            }
            if($modelList == 'model'){
                $this->model($request);
            }
            if($modelList == 'request'){
                $this->request($request);
            }
            if($modelList == 'seederAndFactories'){
                $this->seederAndFactories($request);
            }
            if($modelList == 'routes'){
                $this->routes($request);
            }
            if($modelList == 'appendSeeder'){
                $this->appendSeeder($request);
            }
            if($modelList == 'service'){
                $this->service($request);
            }
            if($modelList == 'controller'){
                $this->controller($request);
            }
            if($modelList == 'appendMenu'){
                $this->appendMenu($request);
            }
            if($modelList == 'view'){
                $this->views($request);
            }

        }
    }

}
