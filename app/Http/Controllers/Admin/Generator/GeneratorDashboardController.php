<?php

namespace App\Http\Controllers\Admin\Generator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GeneratorDashboardController extends Controller
{

    public function index(){
        return view('admin.generator-dashboard.index');
    }

    public function ajaxRenderAll( Request $request)
    {
//        dd($request->all());
        $countsAll = (int)$request->get('counts');
        $type = $request->get('type');
        for ($x = 1; $x < $countsAll; $x++) {
            $counts = $x + 1;
            if (in_array("migration", $type)){
                $response['dataMigration'][] = view('admin/generator-dashboard/render/migrationTemplate', compact('counts'))->render();
                $response['dataMigrationCount'][] = 1;
            }
            if (in_array("seederFactory", $type)){
                $response['dataSeederFactory'][] = view('admin/generator-dashboard/render/seederFactoryTemplate', compact('counts'))->render();
                $response['dataSeederFactoryCount'][] = 1;

            }
            if (in_array("text", $type)){
                $response['dataText'][] = view('admin/generator-dashboard/render/form/textTemplate', compact('counts'))->render();
                $response['dataTextCounts'][] = 1;
            }
        }
//        dd($response);
        return response()->json($response);
    }


    public function ajaxRender( Request $request)
    {
        $counts = $request->get('counts');
        $type = $request->get('type');
        $response['count'] = $counts;
        if($type == 'migration'){
            $response['data'] = view('admin/generator-dashboard/render/'.$type.'Template',compact('counts'))->render();
        }
        elseif ($type == 'seederFactory'){
            $response['data'] = view('admin/generator-dashboard/render/'.$type.'Template', compact('counts'))->render();
        }
        elseif ($type == 'text'){
            $response['data'] = view('admin/generator-dashboard/render/form/'.$type.'Template', compact('counts'))->render();
        }
        elseif ($type == 'email'){
            $response['data'] = view('admin/generator-dashboard/render/form/'.$type.'Template')->render();
        }
        else{
        }
        return response()->json($response);
    }

    public function ajaxSubRender(Request $request)
    {
        $counts = $request->get('counts');
        $response['data'] = view('admin/generator-dashboard/render/form/radioCheckboxTemplate',compact('counts'))->render();
        return response()->json($response);
    }

    public function ajaxImageRender(Request $request)
    {
        $counts = $request->get('counts');
        $response['data'] = view('admin/generator-dashboard/render/form/imageDimensionsTemplate',compact('counts'))->render();
        return response()->json($response);
    }
}
