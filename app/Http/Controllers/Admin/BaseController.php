<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;

class BaseController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirect(Request $request)
    {
        if ($request->has('submit_continue')) {
            return back();
        }
        return redirect()->route( pathinfo($request->route()->getName(), PATHINFO_FILENAME).'.index');
    }

}
