<?php
if (! function_exists('is_active') ) :

    /**
     * Check if given route is active
     *
     * @return boolean
     */
    function is_active(string $route)
    {
        return request()->route()->getName() === $route;
    }

endif;

