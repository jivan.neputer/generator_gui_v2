<?php
namespace App\Http\Controllers\Admin;

use App\Models\{MODEL_NAME};
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use App\Foundation\Base\BaseController;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;
use App\Services\{MODEL_NAME}Service;
use App\Http\Requests\{MODEL_NAME}\{
    StoreRequest,
    UpdateRequest
};
use Illuminate\Http\RedirectResponse;
use Session;

/**
 * Class {MODEL_NAME}Controller
 * @package Foundation\Controllers
 */
class {MODEL_NAME}Controller extends BaseController
{

    /**
     * The {MODEL_NAME}Service instance
     *
     * @var ${VARIABLE_NAME}Service
     */
    private ${VARIABLE_NAME}Service;

    public function __construct({MODEL_NAME}Service ${VARIABLE_NAME}Service)
    {
        $this->{VARIABLE_NAME}Service = ${VARIABLE_NAME}Service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Application|Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return datatables()
                ->of($this->{VARIABLE_NAME}Service->filter($request->input('search.value')))
                ->addColumn('created_at', function ($data) {
                    return $data->created_at . " <code>{$data->created_at->diffForHumans()}</code>";
                })
                ->addColumn('action', function ($data) {
                    $model = '{ROUTE_NAME}';
                    return view('admin.common.datatable.action', compact('data', 'model'))->render();
                })
                ->rawColumns([ 'action', 'created_at', ])
                ->make(true);
        }

        return view('admin.{ROUTE_NAME}.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.{ROUTE_NAME}.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest $request
     * @return RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $imageName = $this->{VARIABLE_NAME}Service->uploadImage($request,'{INPUT_NAME}');
        $this->{VARIABLE_NAME}Service->new($request->except('{COLUMN_NAME}') + ['{COLUMN_NAME}' => $imageName]);
        Session::flash('success','Record successfully created.');
        return $this->redirect($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  {MODEL_NAME} ${VARIABLE_NAME}
     * @return Factory
     */
    public function show({MODEL_NAME} ${VARIABLE_NAME})
    {
        $data = [];
        $data['{ROUTE_NAME}'] = ${VARIABLE_NAME};
        return view('admin.{ROUTE_NAME}.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  {MODEL_NAME} ${VARIABLE_NAME}
     * @return Factory
     */
    public function edit({MODEL_NAME} ${VARIABLE_NAME})
    {
        $data = [];
        $data['{ROUTE_NAME}']  = ${VARIABLE_NAME};
        return view('admin.{ROUTE_NAME}.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  {MODEL_NAME} ${VARIABLE_NAME}
     * @return RedirectResponse
     */
    public function update(UpdateRequest $request, {MODEL_NAME} ${VARIABLE_NAME})
    {
        $dataImageName = $this->{VARIABLE_NAME}Service->getColumnById($request->id,'{INPUT_NAME}')->image;
        //image update
        $imageName = $this->{VARIABLE_NAME}Service->updateImage($request, $dataImageName, '{INPUT_NAME}');
        if($imageName == null){
            $imageName = $dataImageName;
        }
        $this->{VARIABLE_NAME}Service->update($request->except('{COLUMN_NAME}') + ['{COLUMN_NAME}' => $imageName], ${VARIABLE_NAME});
        Session::flash('success','Record successfully updated.');
        return $this->redirect($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  {MODEL_NAME} ${VARIABLE_NAME}
     * @return RedirectResponse
     */
    public function destroy({MODEL_NAME} ${VARIABLE_NAME})
    {
        $dataImageName = $this->{VARIABLE_NAME}Service->getColumnById(${VARIABLE_NAME}->id,'{COLUMN_NAME}')->image;
        $this->{VARIABLE_NAME}Service->removeImage($dataImageName);
        $this->{VARIABLE_NAME}Service->delete(${VARIABLE_NAME});
        Session::flash('success','Record deleted successfully !');
        return redirect()->back();
    }
}
