<?php


namespace App\Generator\Supports\Generator\Handlers;


use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

final class RouteHandler
{
    public $admin = 'admin.php';
    public static function getRoutes($modelName){
        self::checkRouteFileExist();
        $className = BaseHandler::checkInputName($modelName);
        BaseHandler::checkDuplicateEntry($modelName = $className, $fileName = 'route');
        $capName = Str::studly($className);
        $lowName = strtolower($className);
        $routeName = ('Route::resource(\'' . $lowName . "', '{$capName}Controller');\n");
        File::append(base_path('routes/admin.php'), $routeName);
    }

    public static function checkRouteFileExist()
    {
        $route = 'route';
        $path = PathHandler::getPath($route);
        if(!file_exists($path)){
            $myFile = fopen($path, "w");
            $txt = "<?php\n";
            fwrite($myFile, $txt);
            fclose($myFile);
        }
    }

}
