<?php


namespace App\Generator\Supports\Generator\Handlers\View;

use App\Generator\Supports\Generator\Handlers\BaseHandler;

/**
 * Class DataTableColumnHandler
 * @package App\Foundation\Supports\Generator\Handlers
 */
final class DataTableColumnHandler
{
      public static function handle($request)
      {
          $args = [];
          $inputText = $request->get('text');
          $showTable = array_values($inputText['show_table']);
          foreach ($showTable as $inputKey => $inputs) {
              foreach ($inputs as $key => $input) {
                  if ($input == 't_table') {
                      $args[] = $inputText['text_input_name'][$inputKey];
                  }
              }
          }
          return "{data: '" . implode ( "',orderable: true},{data: '", $args ) . "',orderable: true},";
      }
}
