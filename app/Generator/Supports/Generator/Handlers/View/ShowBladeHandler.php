<?php


namespace App\Generator\Supports\Generator\Handlers\View;

use App\Generator\Supports\Generator\Handlers\BaseHandler;

/**
 * Class ShowBladeHandler
 * @package App\Foundation\Supports\Generator\Handlers
 */
class ShowBladeHandler
{

    public static function handle(string $model, $request)
    {
        $args = [];
        $argShow = [];
        $inputText = $request->get('text');
        foreach ($inputText['show_table'] as $inputKey => $inputs) {
            $newKey = $inputKey - 1;
            foreach ($inputs as $key => $input) {
                if ($input == 's_show') {
                    $args[] = $inputText['text_input_name'][$newKey];
                }
            }
        }

        $rows = array_map(function ($column) use ($model) {
            $COLUMN = ucwords(str_replace(['-', '_'], ' ', $column));
            return str_replace([ '{MODEL}', '{model}', '{COLUMN}', '{column}' ], [ ucwords($model) , strtolower($model), $COLUMN, strtolower($column) ],
                app('files')->get(app_path('Generator/Views/row.blade.php')));
        },$args);

    return implode ( "", $rows );
    }
}
