<?php

namespace App\Generator\Supports\Generator\Handlers\View;

use App\Generator\Supports\Generator\Handlers\PathHandler;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

/**
 * Class FormHandler
 * @package App\Foundation\Supports\Generator\Handlers
 */
final class FormHandler
{
    /**
     * full location is " app('files')->get(realpath(base_path('app/Foundation/') " + "Views/Form/text"
     *
     * @var string
     */
    private static $_text = 'Views/Form/text';
    private static $_hidden = 'Views/Form/hidden';
    private static $_month = 'Views/Form/month';
    private static $_textarea = 'Views/Form/textarea';
    private static $_radio = 'Views/Form/Radio/radioContent';
    private static $_email = 'Views/Form/email';
    private static $_select = 'Views/Form/select';
    private static $_password = 'Views/Form/password';
    private static $_number = 'Views/Form/number';
    private static $_file = 'Views/Form/file';
    private static $_date = 'Views/Form/date';
    private static $_range = 'Views/Form/range';
    private static $_tel = 'Views/Form/tel';
    private static $_time = 'Views/Form/time';
    private static $_url = 'Views/Form/url';
    private static $_week = 'Views/Form/week';
    private static $_image = 'Views/Form/image';
    private static $_checkbox = 'Views/Form/Checkbox/checkboxContent';

    public static function handle($request)
    {
        $args = [];
        $placeholder = null;
        $formInputCollection = $request->get('text');
//        dd($request->all());
        foreach ($formInputCollection['input_type'] as $key => $columns) :
            $prop = '_'.strtolower($columns);
            $viewPath = get_class_vars(self::class)['_'.strtolower($columns)];
            if($columns == 'checkbox' || $columns == 'radio') {
                $dataArray = null;
                $dataColl = null;
                $argsCollection = null;
                // TODO if it exist then make it required(label,id,value)
                $checkboxRadioLabel = $formInputCollection['checkbox_radio_label'][$key+1];
                $checkboxRadioId = $formInputCollection['checkbox_radio_id'][$key+1];
                $checkboxRadioDefaultValue = $formInputCollection['checkbox_radio_default_value'][$key+1];

                foreach($formInputCollection['checkbox_radio_label'][$key+1] as $keyCols => $cols){
                    if($keyCols == 0){
                        $dataArray['key'][] = $checkboxRadioLabel[$keyCols];
                        $dataArray['key'][] = $checkboxRadioId[$keyCols];
                        $dataArray['key'][] = $checkboxRadioDefaultValue[$keyCols];
                    }
                    if($keyCols == 1){
                        $dataArray['value'][] = $checkboxRadioLabel[$keyCols];
                        $dataArray['value'][] = $checkboxRadioId[$keyCols];
                        $dataArray['value'][] = $checkboxRadioDefaultValue[$keyCols];
                    }
                }
                $htmlStart ='<div class="hr-line-dashed"></div>
<div class="form-group  row">
    <div class="col-sm-2 ">'.$formInputCollection['text_display_name'][$key].'</div>
        <div class="col-sm-6">';
                $htmlEnd = '</div></div>';
                if ($columns == 'checkbox') {
                    if (array_key_exists($prop, get_class_vars(self::class))) {
                        foreach ($dataArray as $dataArrayKey => $data) :
                            if($dataArrayKey == 'key'){$operation = 'true';}else{$operation = 'false';}
                            $inputNameColl = app('files')->get(realpath(base_path('app/Generator/') . $viewPath . '.blade.php'));
                            $argsCollection[] = str_replace(['columnName', 'statusNum', 'operation', 'idName', 'typeLabel'], [$formInputCollection['text_input_name'][$key], $data[2], $operation, $data[1], $data[0]], $inputNameColl);
                        endforeach;
                        $dataColl = implode(' ' ,$argsCollection);
                        $args[] = $htmlStart.$dataColl. $htmlEnd;
                    }
                }
                if ($columns == 'radio') {
                    if (array_key_exists($prop, get_class_vars(self::class))) {
                        $content = app('files')->get(realpath(base_path('app/Generator/') . $viewPath . '.blade.php'));
                        foreach ($dataArray as $dataArrayKey => $data) :
                            if($dataArrayKey == 'key'){$operation = 'true';}else{$operation = 'false';}
                            $inputNameColl = app('files')->get(realpath(base_path('app/Generator/') . $viewPath . '.blade.php'));
                            $argsCollection[] = str_replace(['columnName', 'statusNum', 'operation', 'idName', 'typeLabel'], [$formInputCollection['text_input_name'][$key], $data[2], $operation, $data[1], $data[0]], $inputNameColl);
                        endforeach;
                        $dataColl = implode(' ' ,$argsCollection);
                        $args[] = $htmlStart.$dataColl. $htmlEnd;
                    }
                }
            }
            else{
                if (array_key_exists($prop, get_class_vars(self::class))) {
                    $content = app( 'files')->get(realpath(base_path('app/Generator/').$viewPath.'.blade.php'));
                    $labelName = $formInputCollection['text_display_name'][$key];
                    $inputName = $formInputCollection['text_input_name'][$key];
                    if(!is_null($formInputCollection['text_placeholder'][$key])){
                        $placeholder = "'placeholder'=>'".$formInputCollection['text_placeholder'][$key]."'";
                    }
                    $args[] = str_replace([ 'labelName', 'columnName', 'placeholder'], [ $labelName, $inputName, $placeholder], $content);
                }
            }
        endforeach;
        return implode(PHP_EOL, $args);
    }


    /**
     * @param $column
     * @param $colKey
     * @param $input
     */
    public static function solveColumn($column, $colKey, $input){
        if( $input == 'radio') {$fileName = 'radioContent';} else{$fileName = 'checkboxContent';}
        $content = app('files')->get( self::getDirectory($folderName = $input , $fileName ));

        $expDatas = explode('=>', $column);
        foreach ($expDatas as $expKey => $singleData) {
            if ($expKey > 0) {
                $expSingleData = explode('->', $singleData);
                $capExpSingleData = ucwords($expSingleData[0]);
                if(is_numeric($expSingleData[1])){
                    if($expSingleData[1] == 1){
                        $trueFalse = "true";
                    }
                    else{
                        $trueFalse = "false";
                    }
                    $statusNum = $expSingleData[1];
                }
                if(!is_numeric($expSingleData[1])){
                    if($expKey == 1){
                        $trueFalse = "true";
                    }
                    else{
                        $trueFalse = "false";
                    }
                    $statusNum = "'$expSingleData[1]'";
                }

                $totalContent[] = str_replace(
                    [ 'type', 'label', 'statusNum', 'operation', 'idType'],
                    [ $expSingleData[0], $capExpSingleData, $statusNum, $trueFalse, $expSingleData[0]],
                    $content);

            }
        }

        self::createFile( $totalContent, $colKey, $input);

    }


    /**
     * @param $totalContent
     * @param $colKey
     * @param $input
     */
    public static function createFile($totalContent, $colKey, $input){
        if( $input == 'radio') {$fileName = 'radioTemplate';$virtualFileName = 'virtualRadio';}
        else{$fileName = 'checkboxTemplate';$virtualFileName = 'virtualCheckbox';}
        $fileContent = file_get_contents( self::getDirectory($folderName = $input, $fileName ));
        $radioData = str_replace(
            [
                '{AllContent}'
            ],
            [
                implode('', $totalContent)
            ],
            $fileContent
        );

        file_put_contents(
            self::getDirectory($folderName = $input, $fileName = $virtualFileName.$colKey),
            $radioData
        );
    }

    /**
     * @param $string
     * @param $symbol
     * @return false|int
     */
    public static function stringPosition($string, $symbol ){
        return strpos($string, $symbol);
    }

    /**
     * @param $folderName
     * @param $fileName
     * @return string
     */
    public static function getDirectory($folderName, $fileName){
        return PathHandler::getPath($folderName) . DIRECTORY_SEPARATOR . $fileName . '.blade.php';
    }

    /**
     * @param $input
     */
    public static function unlinkFile($input){
     foreach ($input as $file){
         if($file == 'radio'){$fileName = 'virtualRadio*.*';}else{$fileName = 'virtualCheckbox*.*';}
         $path = PathHandler::getPath($file). DIRECTORY_SEPARATOR;
         foreach (glob($path .$fileName) as $files) {
             unlink($files);
         }
     }
    }

    public static function processForm($data)
    {
        dd($data);
    }

}


