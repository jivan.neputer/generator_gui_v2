<?php
namespace App\Generator\Supports\Generator\Handlers\View;

use App\Generator\Supports\Generator\Handlers\BaseHandler;

/**
 * Class TableHeadHandler
 * @package App\Foundation\Supports\Generator\Handlers
 */
class TableHeadHandler
{
    public static function handle($request)
    {

        $args = [];
        $inputText = $request->get('text');
        foreach ($inputText['show_table'] as $inputKey => $inputs) {
            $newKey = $inputKey - 1;
            foreach ($inputs as $key => $input) {
                if ($input == 't_table') {
                    $args[] = $inputText['text_input_name'][$newKey];
                }
            }
        }
        return "<th>" . implode ( "</th><th>", $args ) . "</th>";
    }

}
