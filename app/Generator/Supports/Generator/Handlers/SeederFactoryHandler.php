<?php


namespace App\Generator\Supports\Generator\Handlers;


use Illuminate\Support\Str;

final class seederFactoryHandler
{
    public static function getSeederFactory($modelName, $column)
    {
        $className = BaseHandler::checkInputName($modelName);
        BaseHandler::checkDuplicateEntry($modelName = $className, $fileName = 'factory');

        $fileName = Str::studly($className);
        $seederTemplate = self::solveStub($fileName, $fileType = 'Seeder');
        $column = self::reArrangeColumn($column);
        $factoryTemplate = self::solveStubContain($fileName, $fileType = 'Factory', $column);

        $rootPath = base_path().DIRECTORY_SEPARATOR.'database'.DIRECTORY_SEPARATOR;
        $seederPath = $rootPath.'seeds';
        $factoryPath = $rootPath.'factories';

        BaseHandler::makeFile($seederPath, $fileName, $fileExtension = 'TableSeeder.php', $seederTemplate);
        BaseHandler::makeFile($factoryPath, $fileName, $fileExtension = 'Factory.php', $factoryTemplate);

    }

    public static function reArrangeColumn($columns)
    {
        $rowArg = [];
        try {
            foreach ($columns as $keys => $column) {
                //check for password and now case
                if($column[1] == 'password'){
                    $rowArg[] = "'".$column[0]."'" ." => "."'".'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'."'". ',';
                }
                else{
                    $rowArg[] = "'".$column[0]."'" ." => ". '$faker->' . $column[1].',';
                }
            }
            return implode(PHP_EOL, $rowArg );
        }
        catch (\Exception $ex){
            return 'Error in migration file process';
        }
    }

    public static function solveStub($capitalVariableName, $fileType){
        return str_replace(
            [
                '{modelName}',
            ],
            [
                $capitalVariableName,
            ],
            BaseHandler::getStub($fileType)
        );
    }

    public static function solveStubContain($capitalVariableName, $fileType, $modelAttributes){
        return str_replace(
            [
                '{modelName}',
                '{FILLABLE_ATTR}',
            ],
            [
                $capitalVariableName,
                $modelAttributes,
            ],
            BaseHandler::getStub($fileType)
        );
    }
}
