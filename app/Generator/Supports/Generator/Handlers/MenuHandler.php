<?php


namespace App\Generator\Supports\Generator\Handlers;


final class MenuHandler
{
    public static function handle($modelName)
    {
        $className = BaseHandler::checkInputName($modelName);
        BaseHandler::checkDuplicateEntry($modelName = $className, $fileName = 'appendMenu');

        $content = app('files')->get(resource_path('views/admin/layouts/menu.blade.php'));
        $menuTitle = str_replace('-',' ',$className);

        $menu = str_replace([ '{MENU}', '{menu}' ], [ ucwords($menuTitle) ,strtolower($className) ],
            app('files')->get(app_path('Generator/Views/menu.blade.php')));

        $content = $content . PHP_EOL . $menu;
        file_put_contents(
            resource_path('views/admin/layouts/menu.blade.php'),
            $content
        );
    }
}
