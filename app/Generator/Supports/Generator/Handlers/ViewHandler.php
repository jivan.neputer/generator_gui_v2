<?php


namespace App\Generator\Supports\Generator\Handlers;


use App\Generator\Supports\Generator\Handlers\View\DataTableColumnHandler;
use App\Generator\Supports\Generator\Handlers\View\FormHandler;
use App\Generator\Supports\Generator\Handlers\View\ShowBladeHandler;
use App\Generator\Supports\Generator\Handlers\View\TableHeadHandler;
use Illuminate\Support\Str;

final class ViewHandler
{
    public static function handle($request)
    {
        self::operateImage($request);
        $modelName = $request->get('modelName');
//        $viewsCollection = ['create', 'edit', 'form', 'index', 'table', 'show'];
        $viewsCollection = ['create', 'edit', 'form', 'index', 'script', 'show', 'table'];
        $className = BaseHandler::checkInputName($modelName);
        $rootPath = base_path().DIRECTORY_SEPARATOR.'resources'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR;
        BaseHandler::makeDirectory($rootPath , $folderName = 'admin');
        $adminPath = $rootPath.DIRECTORY_SEPARATOR.'admin';

        BaseHandler::makeDirectory($adminPath , $folderName = $className);
        $folderPath = $adminPath.DIRECTORY_SEPARATOR.$className;
        $extension = '.blade.php';

        foreach ($viewsCollection as $key => $views) :
            $templateName = self::solveViewStub($request, $className, $fileName = $views);

            if ($views == 'form' || $views == 'script' || $views == 'table'):
                BaseHandler::makeDirectory($folderPath , $fileName = 'partials');
                $partialPath = $folderPath.DIRECTORY_SEPARATOR.'partials';
                BaseHandler::makeFile($partialPath, $views, $fileType = $extension, $templateName);
            else:
                BaseHandler::makeFile($folderPath, $views, $fileType = $extension, $templateName);
            endif;
        endforeach;
    }

    public static function operateImage($request)
    {
        $modelName = $request->get('modelName');
        $input = $request->get('text');
        $allInputType = $input['input_type'];
        if (in_array("file", $allInputType)){
            $dimensions = $input['dimensions'];
            // check file exist or not
            $path =  base_path() . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'image_dimensions.php';
            $data = null;
            $dt = null;
            $image_dimensions_width = array_filter(call_user_func_array('array_merge', $dimensions['image_dimensions_width']));
            $image_dimensions_height = array_filter(call_user_func_array('array_merge', $dimensions['image_dimensions_height']));
            $image_dimensions_quality = array_filter(call_user_func_array('array_merge', $dimensions['image_dimensions_quality']));
            foreach ($image_dimensions_width as $key => $width){
                if( !next( $image_dimensions_width ) ) {
                    $data .= "['width' => ".$width.", 'height' => ".$image_dimensions_height[$key].", 'quality' => ".$image_dimensions_quality[$key]."],";
                }else{
                    $data .= "['width' => ".$width.", 'height' => ".$image_dimensions_height[$key].", 'quality' => ".$image_dimensions_quality[$key]."],\n";
                }
            }
            $modelDimension = "'$modelName' => [ \n $data \n ],";
            if(file_exists($path)){
                // update file
                $content = app('files')->get($path);
                $explodeContent=explode('];', $content);
                $dt = $explodeContent[0].$modelDimension."\n".'];';
                file_put_contents(
                    $path,
                    $dt
                );
            }else{
                // create file
$dimension = "<?php
    /**
     *  image dimension
     */
    return[
            '".$modelName."' => [
                $data
            ],
    ];";

                $fp = fopen($path,"wb");
                fwrite($fp,$dimension);
                fclose($fp);
            }
        }
    }

    public static function solveViewStub($request, $className, $fileName)
    {
        $inputCount = $request->get('text');
        $tableInput = array_column($inputCount['show_table'], 1);
        $tableInputCount = count($tableInput);
        $inputCount = count($inputCount['text_display_name']);
        if($fileName == 'form'):
            $dynamicForm = FormHandler::handle($request);

            return str_replace(
                [
                    '{DYNAMIC_FORM}',
                ],
                [
                    $dynamicForm,
                ],
                BaseHandler::getViewStub($fileName)
            );
        elseif($fileName == 'index'):
            $dataTableColumnHandler = DataTableColumnHandler::handle($request);
            return str_replace(
                [
                    '{modelName}',
                    '{LOWER_CLASS_NAME}',
                    '{CLASS_NAME}',
                    '{DATATABLE_COLUMNS}',
                    '{count}',
                ],
                [
                    $className,
                    strtolower($className),
                    Str::studly($className),
                    $dataTableColumnHandler,
                    $tableInputCount,
                ],
                BaseHandler::getViewStub($fileName)
            );
        elseif($fileName == 'table'):
            $tableHeader = TableHeadHandler::handle($request);
            return str_replace(
                [
                    '{TABLE_HEADS}',
                ],
                [
                    $tableHeader,
                ],
                BaseHandler::getViewStub($fileName)
            );
        elseif($fileName == 'show'):
            $showRows = ShowBladeHandler::handle($className, $request);
            return str_replace(
                [
                    '{SHOW_ROWS}',
                    '{LOWER_CLASS_NAME}',
                    '{modelName}'
                ],
                [
                    $showRows,
                    strtolower($className),
                    $className
                ],
                BaseHandler::getViewStub($fileName)
            );
        else:
            return str_replace(
                [
                    '{modelName}',
                    '{LOWER_CLASS_NAME}'
                ],
                [
                    $className,
                    strtolower($className),
                ],
                BaseHandler::getViewStub($fileName)
            );
        endif;
    }

}
