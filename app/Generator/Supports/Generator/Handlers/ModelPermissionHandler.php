<?php


namespace App\Generator\Supports\Generator\Handlers;


final class ModelPermissionHandler
{
    public static function handle($request)
    {
        $modelName = [];
        $checkPermissionStatus = $request->get('permission');
        foreach($checkPermissionStatus as $key => $PermissionStatus) {
            if($PermissionStatus == true){
                if($key == 'migration-permission'){
                    $modelName[] = 'migration';
                }
                if($key == 'model-permission'){
                    $modelName[] = 'model';
                }
                if($key == 'request-permission'){
                    $modelName[] = 'request';
                }
                if($key == 'seederAndFactories-permission'){
                    $modelName[] = 'seederAndFactories';
                }
                if($key == 'service-permission'){
                    $modelName[] = 'service';
                }
                if($key == 'routes-permission'){
                    $modelName[] = 'routes';
                }
                if($key == 'controller-permission'){
                    $modelName[] = 'controller';
                }
                if($key == 'view-permission'){
                    $modelName[] = 'view';
                }
                if($key == 'appendMenu-permission'){
                    $modelName[] = 'appendMenu';
                }
                if($key == 'appendSeeder-permission'){
                    $modelName[] = 'appendSeeder';
                }
            }
        }
        return $modelName;
    }
}
