<?php


namespace App\Generator\Supports\Generator\Handlers;


use Illuminate\Support\Str;

final class RequestHandler
{
    public static function getRequest($modelName)
    {
        $className = BaseHandler::checkInputName($modelName);
        BaseHandler::checkDuplicateEntry($modelName = $className, $fileName = 'request');
        $fileName = Str::studly($className);
        $storeRequestTemplate = self::solveStub($fileName, $fileType = 'StoreRequest');
        $updateRequestTemplate = self::solveStub($fileName, $fileType = 'UpdateRequest');

        $rootPath = app_path().DIRECTORY_SEPARATOR.'Http'.DIRECTORY_SEPARATOR;
        BaseHandler::makeDirectory($rootPath , $fileType = 'Requests', $fileName);
        $folderPath = $rootPath.DIRECTORY_SEPARATOR.'Requests'.DIRECTORY_SEPARATOR.$fileName;

        BaseHandler::makeFile($folderPath, $requestType = 'Store', $fileExtension = 'Request.php', $storeRequestTemplate);
        BaseHandler::makeFile($folderPath, $requestType = 'Update', $fileExtension = 'Request.php', $updateRequestTemplate);
    }

    public static function solveStub($modelName, $fileName){
        return str_replace(
            ['{modelName}'], [$modelName],
            BaseHandler::getStub($fileName)
        );
    }
}
