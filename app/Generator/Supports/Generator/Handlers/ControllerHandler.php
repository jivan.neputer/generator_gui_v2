<?php


namespace App\Generator\Supports\Generator\Handlers;


use Illuminate\Support\Str;

final class ControllerHandler
{
    public static function getController($request)
    {
        $modelName = $request->get('modelName');
        $input = $request->get('text');
        $allInputType = $input['input_type'];


        $className = BaseHandler::checkInputName($modelName);
        BaseHandler::checkDuplicateEntry($modelName = $className, $fileName = 'controller');
        $fileName = Str::studly($className);
        if (in_array("file", $allInputType)) {
            $key = array_search ('file', $allInputType);
            $allColumnName = $input['text_input_name'];
            $controllerTemplate = self::solveStubWithImage($className, $fileType = 'ControllerImage', $allColumnName[$key]);
        }else{
            $controllerTemplate = self::solveStub($className, $fileType = 'Controller');
        }
        $rootPath = app_path().DIRECTORY_SEPARATOR.'Http'.DIRECTORY_SEPARATOR.'Controllers'.DIRECTORY_SEPARATOR;
        BaseHandler::makeDirectory($rootPath , $folderName = 'Admin');
        $folderPath = $rootPath.DIRECTORY_SEPARATOR.'Admin';
        BaseHandler::makeFile($folderPath, $fileName, $fileType = 'Controller.php', $controllerTemplate);
    }

    public static function solveStubWithImage($className, $fileType, $columnName){
        return str_replace(
            [
                '{MODEL_NAME}',
                '{VARIABLE_NAME}',
                '{ROUTE_NAME}',
                '{COLUMN_NAME}',
                '{INPUT_NAME}',
            ],
            [
                Str::studly($className),
                lcfirst(Str::studly($className)),
                $className,
                $columnName,
                $columnName,
            ],
            BaseHandler::getStub($fileType)
        );
    }

    public static function solveStub($className, $fileType){
        return str_replace(
            [
                '{MODEL_NAME}',
                '{VARIABLE_NAME}',
                '{ROUTE_NAME}',
            ],
            [
                Str::studly($className),
                lcfirst(Str::studly($className)),
                $className,
            ],
            BaseHandler::getStub($fileType)
        );
    }
}
