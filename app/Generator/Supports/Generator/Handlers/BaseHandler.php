<?php


namespace App\Generator\Supports\Generator\Handlers;


use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

final class BaseHandler
{

    /**
     * @param $type
     * @return false|string
     */
    public static function getStub($type){
        return file_get_contents(app_path("/Generator/Supports/Generator/Console/stubs/$type.stub"));
    }

    /**
     * @param $type
     * @return false|string
     */
    public static function getViewStub($type){
        return file_get_contents(app_path("/Generator/Supports/Generator/Console/stubs/views/$type.blade.stub"));
    }

    /**
     * @param $folderPath
     * @param $className
     * @param $fileType
     * @param $requestTemplate
     */
    public static function makeFile($folderPath, $className, $fileType, $requestTemplate){
        file_put_contents( $folderPath.DIRECTORY_SEPARATOR.$className.$fileType, $requestTemplate);
    }

    /**
     * @param $rootPath
     * @param $fileType
     * @param null $className
     */
    public static function makeDirectory($rootPath, $fileType, $className = null){
        if(!file_exists($path = $rootPath.DIRECTORY_SEPARATOR.$fileType)){
            mkdir($path, 0777, true);
        }
        if($className != null){
            if(!file_exists($folderPath = $path.DIRECTORY_SEPARATOR.$className)){
                mkdir($folderPath, 0777, true);
            }
        }
    }

    public static function checkFolderExists($path)
    {
        if(file_exists($path))
            return true;
        else
            return false;
    }
    /**
     * @param $migration
     * @return mixed|string
     */
    public static function checkInputName($migration): string
    {
        if(preg_match('/[\'^£$%&*()}{@#~?><>,|=+¬_]/', $migration)){
            dd('Your input name should be like this : category or menu-category');
        }
        if(strtolower($migration) !== $migration){
            dd('Your input name should be in lower case like this : category or menu-category');
        }
        if (preg_match('/[\s]+/', $migration)) {
            dd('Your model name should not have space like this : category or menu-category');
        }
        return $migration;
    }

    /**
     * @param $modelName
     * @param $fileName
     */
    public static function checkDuplicateEntry($modelName,$fileName){
        if($fileName == 'migration'){
            self::checkDuplicateMigration($fileName,$modelName);
        }
        if($fileName == 'model'){
            self::checkDuplicateModel($fileName,$modelName);
        }

    }

    /**
     * @param $fileName
     * @param $modelName
     */
    public static function checkDuplicateMigration($fileName,$modelName)
    {
        $path = PathHandler::getPath($fileName);
        $migrationFiles = self::getFile($path);
        if($migrationFiles != false){
            foreach ($migrationFiles as $migration){
                $tableName = explode('_create_', $migration);
                if($tableName[1] == $modelName.'s_table'){
                    dd($modelName.' migration is already exist!');
                }
            }
        }
    }

    /**
     * @param $fileName
     * @param $modelName
     */
    public static function CheckDuplicateModel($fileName,$modelName)
    {
        $path = PathHandler::getPath($fileName);
        $resolveModelName = Str::studly($modelName);
        $modelFiles = self::getFile($path);
        if($modelFiles != false){
            if(in_array($resolveModelName, $modelFiles)){
                dd($modelName.' model is already exist!');
            }
        }
    }

    /**
     * @param $path
     * @return array|false
     */
    public static function getFile($path)
    {
        $modelFiles = [];
        $pathStatus = self::checkFolderExists($path);
        if($pathStatus == true){
            $allFiles = File::files($path);
            foreach ($allFiles as $key => $files){
                $file = pathinfo($files);
                $modelFiles[] = $file['filename'];
            }
            return $modelFiles;

        }else{
            return false;
        }
    }



}
