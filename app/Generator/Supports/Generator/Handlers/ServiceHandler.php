<?php


namespace App\Generator\Supports\Generator\Handlers;


use Illuminate\Support\Str;

final class ServiceHandler
{
    public static function getService($request)
    {
        $modelName = $request->get('modelName');
        $input = $request->get('text');
        $allInputType = $input['input_type'];

        $className = BaseHandler::checkInputName($modelName);
        BaseHandler::checkDuplicateEntry($modelName = $className, $fileName = 'service');
        $fileName = Str::studly($className);
        $variableName = lcfirst($fileName);
        if (in_array("file", $allInputType)) {
            $serviceTemplate = self::solveServiceStub($fileName, $fileType = 'ServiceImage', $variableName);
        }else{
            $serviceTemplate = self::solveServiceStub($fileName, $fileType = 'Service', $variableName);
        }
        $rootPath = app_path().DIRECTORY_SEPARATOR;
        BaseHandler::makeDirectory($rootPath , $fileType = 'Services');
        $folderPath = $rootPath.DIRECTORY_SEPARATOR.'Services';
        BaseHandler::makeFile($folderPath, $fileName, $fileExtension = 'Service.php', $serviceTemplate);

    }

    public static function solveServiceStub($modelName, $fileName, $variableName){
        return str_replace([
            '{MODEL_NAME}',
            '{VAR_CLASS_NAME}'
        ],
            [
                $modelName,
                $variableName,
            ],
            BaseHandler::getStub($fileName)
        );
    }
}
