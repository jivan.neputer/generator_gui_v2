<?php


namespace App\Generator\Supports\Generator\Handlers;


use Illuminate\Support\Str;

final class seederHandler
{
    public static function appendSeeder($modelName){
        $className = BaseHandler::checkInputName($modelName);
        BaseHandler::checkDuplicateEntry($modelName = $className, $fileName = 'appendSeeder');
        $seederName = self::resolveSeederName($className);
        $content = app('files')->get(base_path('database/seeds/DatabaseSeeder.php'));
        $data = str_replace(['}'],[''],$content);
        $final = rtrim($data) . $seederName;
        file_put_contents(
            base_path('database/seeds/DatabaseSeeder.php'),
            $final
        );
    }

    public static function resolveSeederName($seederName){
        $data = Str::studly($seederName).'TableSeeder';
        $seederClass = '$this->call('.$data.'::class);';
        return PHP_EOL.'        '.$seederClass. PHP_EOL. '   }'. PHP_EOL. '}';
    }

}
