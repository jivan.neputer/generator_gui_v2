<?php


namespace App\Generator\Supports\Generator\Handlers;


use Illuminate\Support\Str;

final class MigrationHandler
{
    public static function getMigration($migration,$columns)
    {
        $className = BaseHandler::checkInputName($migration);
        BaseHandler::checkDuplicateEntry($modelName = $className, $fileName = 'migration');
        $tableName = self::resolveMigrationClassName($className);
        $fileName = self::resolveFileName($tableName);
        $migrationContain = self::getMigrationContain($columns);
        $migrationTemplate = self::solveStub($tableName, $migrationContain, $fileType = 'Migration');
        $folderPath = base_path().DIRECTORY_SEPARATOR.'database'.DIRECTORY_SEPARATOR.'migrations';
        BaseHandler::makeFile($folderPath, $fileName, $fileExtension = '.php', $migrationTemplate);
    }

    public static function resolveMigrationClassName($className){
        $name = Str::plural(strtolower($className));
        return str_replace('-','_',$name);
    }

    public static function resolveFileName($tableName): string
    {
        $year = now()->year;
        $month = now()->format('m');
        $day = now()->format('d');
        $time = now()->format('his');
        return $year.'_'.$month.'_'.$day.'_'.$time.'_create_'.$tableName.'_table';
    }

    public static function getMigrationContain($columns)
    {
        $rowArg = [];
        $previousKey = null;
        try {
            foreach ($columns as $keys => $column) {
                $row = '$table->' . $column[1] . "('" . $column[0] . "')";

                if (!is_null($column[2])) {
                    if ($column[2] == 'unique' || $column[2] == 'nullable' ) {
                        $row = $row . "->" . $column[2] . '()';
                    }
                }
                if (!is_null($column[2])) {
                    if ($column[2] == 'default') {
                        $row = $row . "->" . $column[2];
                    }
                }

                if (!is_null($column[3])) {
                    if(is_numeric($column[3])){
                        $row = $row."(" . $column[3] . ")";
                    }
                    else{
                        $row = $row."('" . $column[3] . "')";
                    }
                }
                if (!is_null($column[4])) {
                        $row = $row."->comment"."('" . $column[4] . "')";
                    }
                $rowArg[] = strtolower($row).';';
            }
            return implode(PHP_EOL, $rowArg );
        }
        catch (\Exception $ex){
            return 'Error in migration file process';
        }
    }

    public static function solveStub($className, $migrationContain, $fileType){
        $file = BaseHandler::getStub($fileType);
        $variables = ['{MODEL_NAME}','{FILLABLE_ATTR}','{TABLE_NAME}'];
        $values = [Str::studly($className),$migrationContain,$className];
        return str_replace($variables,$values,$file);
    }
}
