<?php


namespace App\Generator\Supports\Generator\Handlers;


use Illuminate\Support\Str;

final class FillableHandler
{
    public static function getFillable($modelName, $fillable)
    {
        $className = BaseHandler::checkInputName($modelName);
        BaseHandler::checkDuplicateEntry($modelName = $className, $fileName = 'model');
        $fileName = Str::studly($className);
        $modelAttributes = self::getColumn($fillable);
        $modelTableType = self::checkModelType($className);
        if($modelTableType == true):
            $protectedModel = self::resolveMultiClassName($className);
            $modelTemplate = self::solveStubProtectedModelContain($fileName, $fileType = 'ModelProtected', $modelAttributes, $protectedModel);
        else:
            $modelTemplate = self::solveStubContain($fileName, $filetype = 'Model', $modelAttributes);
        endif;

        $rootPath = app_path().DIRECTORY_SEPARATOR;
        BaseHandler::makeDirectory($rootPath , $fileType = 'Models');
        $folderPath = $rootPath.DIRECTORY_SEPARATOR.'Models';
        if($modelTableType == true):
            BaseHandler::makeFile($folderPath, $fileName, $fileExtension = '.php', $modelTemplate);
        else:
            BaseHandler::makeFile($folderPath, $fileName, $fileExtension = '.php', $modelTemplate);
        endif;
    }

    public static function getColumn($fillable)
    {
        $arrayColumn = [];
        $fillableArray = explode(',',$fillable);
        foreach ($fillableArray as $key => $value){
            $val = trim($value);
            $arrayColumn[] = Str::slug($val, "_");
        }
        $trimmed_array = array_filter($arrayColumn);
        return  "'" . implode ( "', '", $trimmed_array ) . "'";
    }

    public static function resolveMultiClassName($className){
        $name = Str::plural(strtolower($className));
        return str_replace('-','_',$name);
    }

    public static function solveStubProtectedModelContain($capitalVariableName, $fileType, $modelAttributes,$protectedModel){
        return str_replace(
            [
                '{modelName}',
                '{FILLABLE_ATTR}',
                '{protectedModel}',
            ],
            [
                $capitalVariableName,
                $modelAttributes,
                $protectedModel,
            ],
            BaseHandler::getStub($fileType)
        );
    }

    public static function solveStubContain($capitalVariableName, $fileType, $modelAttributes){
        return str_replace(
            [
                '{modelName}',
                '{FILLABLE_ATTR}',
            ],
            [
                $capitalVariableName,
                $modelAttributes,
            ],
            BaseHandler::getStub($fileType)
        );
    }

    public static function checkModelType($className){
        if(strpos($className, '-') !== false):
            return true;
        else:
            return false;
        endif;
    }

}
