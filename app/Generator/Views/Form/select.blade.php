<div class="hr-line-dashed"></div>
<div class="form-group  row">
    <label class="col-sm-2 col-form-label is_required">labelName</label>
    <div class="col-sm-6 {{ $errors->has('columnName') ? 'has-error' : '' }}">
        {!! Form::select('columnName[]', $data['columnName'],null, ['class' => 'form-control', 'multiple']) !!}
{{--        echo Form::select('size', ['L' => 'Large', 'S' => 'Small']);--}}
{{--        echo Form::select('size', ['L' => 'Large', 'S' => 'Small'], 'S');--}}
{{--        echo Form::selectMonth('month');--}}
{{--        echo Form::selectRange('number', 10, 20);--}}
{{--        {!! Form::select('basic[availability_type]', ['Full Time' => 'Full Time', 'Part Time' => 'Part Time', 'Contract Based' => 'Contract Based'], null, ['placeholder' => 'Select Availability', 'class' => 'form-control']) !!}--}}
        {{--        @error('basic.availability_type')--}}
    @if($errors->has('columnName'))
            <label class="has-error" for="columnName">{{ $errors->first('columnName') }}</label>
        @endif
    </div>
</div>
