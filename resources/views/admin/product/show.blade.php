@extends('admin.layouts.layout')

@push('style')

@endpush

@section('title', 'product')

@section('content')

@include('admin.common.breadcrumbs', [
'title' => 'Show',
'panel' => 'product',
'id'    => $data['product']->id,
])

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">

                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th width="150px">Label</th>
                            <th>Information</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
    <td style="width:35%;">Name</td>
    <td style="width:65%">{{ $data['product']->name }}</td>
</tr>
<tr>
    <td style="width:35%;">Image</td>
    <td style="width:65%">{{ $data['product']->image }}</td>
</tr>
<tr>
    <td style="width:35%;">Number</td>
    <td style="width:65%">{{ $data['product']->number }}</td>
</tr>
<tr>
    <td style="width:35%;">Status</td>
    <td style="width:65%">{{ $data['product']->status }}</td>
</tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')

@endpush
