<script>
    $(function () {
        $("#productForm").validate({
            rules: {
                name: "required",
            },
            messages: {
                name: "Please enter product name",
            }
        });
    })
</script>
