<div class="hr-line-dashed"></div>
<div class="form-group  row">
    <label class="col-sm-2 col-form-label is_required">your name</label>
    <div class="col-sm-6 {{ $errors->has('name') ? 'has-error' : '' }}">
        {!! Form::text('name', null, [ 'class' => 'form-control', 'placeholder'=>'your name' ]) !!}
        @if($errors->has('name'))
            <label class="has-error" for="name">{{ $errors->first('name') }}</label>
        @endif
    </div>
</div>

<div class="hr-line-dashed"></div>
<div class="form-group  row">
    <label class="col-sm-2 col-form-label is_required">image</label>
    <div class="col-sm-6 {{ $errors->has('image') ? 'has-error' : '' }}">
        {!! Form::file('image', [ 'class' => 'form-control' ]) !!}
        @if($errors->has('image'))
            <label class="has-error" for="image">{{ $errors->first('image') }}</label>
        @endif
    </div>
</div>

<div class="hr-line-dashed"></div>
<div class="form-group  row">
    <label class="col-sm-2 col-form-label is_required">number</label>
    <div class="col-sm-6 {{ $errors->has('number') ? 'has-error' : '' }}">
        {!! Form::number('number', null,['min'=>0,'class' => 'form-control', 'placeholder'=>'your number']) !!}
        @if($errors->has('number'))
            <label class="has-error" for="number">{{ $errors->first('number') }}</label>
        @endif
    </div>
</div>

<div class="hr-line-dashed"></div>
<div class="form-group  row">
    <div class="col-sm-2 ">status</div>
        <div class="col-sm-6"><label for="active">Active</label>
{!! Form::checkbox('status', 1, true, ['id' => 'active']) !!}
 <label for="in-active">in-active</label>
{!! Form::checkbox('status', 0, false, ['id' => 'in-active']) !!}
</div></div>
