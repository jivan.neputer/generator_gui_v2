@extends('admin.layouts.layout')

@push('style')
@endpush

@section('title', 'Edit a product')

@section('content')

    @include('admin.common.breadcrumbs', [
        'title'=> 'Edit',
        'panel'=> 'product',
        'id'=> $data['product']->id,
    ])

    <div class="wrapper wrapper-content">
        {!! Form::model($data['product'],['route' => ['admin.product.update',$data['product']->id], 'enctype' => 'multipart/form-data', 'method' => 'put', 'id' => 'productForm']) !!}
        {!! Form::hidden('id', $data['product']->id) !!}
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>General Info</h5>
                    </div>
                    <div class="ibox-content">
                        @includeIf('admin.product.partials.form')
                    </div>
                </div>
            </div>

        </div>
        @includeIf('admin.common.action')
        {!! Form::close() !!}
    </div>

@endsection

@push('js')
    <script src="{{ asset('assets/admin/plugins/validate/jquery.validate.min.js') }}"></script>
    @include('admin.product.partials.script')
@endpush
