@extends('admin.layouts.layout')

@push('style')
@endpush

@section('title', 'Create a product')

@section('content')
    @include('admin.common.breadcrumbs', [
        'title'=> 'Create',
        'panel'=> 'product',
    ])

    <div class="wrapper wrapper-content">
        {!! Form::open(['route' => 'admin.product.store', 'enctype' => 'multipart/form-data', 'method' => 'post', 'id' => 'productForm']) !!}
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>General Info</h5>
                    </div>
                    <div class="ibox-content">
                        @include('admin.product.partials.form')
                    </div>
                </div>
            </div>
        </div>
        @includeIf('admin.common.action')
        {!! Form::close() !!}
    </div>
@endsection

@push('js')
    <script src="{{ asset('assets/admin/plugins/validate/jquery.validate.min.js') }}"></script>
    @include('admin.product.partials.script')
@endpush
