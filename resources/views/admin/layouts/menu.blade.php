<li class="{{ (request()->is('admin/product*')) ? 'active' : '' }}">
    <a href="#" aria-expanded="false">
        <i class="fa fa-bar-chart-o"></i> <span class="nav-label">Product </span><span class="fa arrow"></span>
    </a>
    <ul class="nav nav-second-level collapse" aria-expanded="false">
        <li class="{{ (request()->is('admin/product')) ? 'active' : '' }}"><a href="{{ route('admin.product.index') }}">List</a></li>
        <li class="{{ (request()->is('admin/product/create')) ? 'active' : '' }}"><a href="{{ route('admin.product.create') }}">Add</a></li>
    </ul>
</li>


