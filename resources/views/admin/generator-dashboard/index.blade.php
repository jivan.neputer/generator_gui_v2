@extends('admin.layouts.layout')

@push('style')
    <link href="{{ asset('assets/admin/css/select2.min.css') }}" rel="stylesheet">
    <style>
        .card-header>h5>span:hover{cursor: pointer;}
        .plus-icon>i{margin-right: -30px;}
        .text-input-field-wrapper{background-color: #f3f3f4;padding: 30px 20px;border-bottom: 1px solid #fefefe;}
        /*.permission div {margin-left: 0;}*/
        .permission div label{text-align: left;align-items: normal;justify-content: flex-start;}
        .permission div input{text-align: right;align-items: normal;justify-content: flex-start;}
        .select2-container .select2-selection--single {height: 30px !important; width: 195px !important;}
        .select2-container--default .select2-selection--single .select2-selection__arrow {right: -25px !important;}
        .text-input-field-wrapper > .form-group{margin-bottom: 0;}
        .radioCheckbox { margin-top: 1px;}
        .pull-right-sub{ position: absolute;left: 98%; }
    </style>
@endpush
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
    @include('admin.generator-dashboard.message')
    <form action="{{ route('admin.generator.create') }}" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
        @csrf

        @includeIf('admin.generator-dashboard.form.formCollection')
        <br>
        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button type="submit" name="save" value="true" class="btn btn-success">Save</button>
                <button type="submit" class="btn btn-success">Save & Continue</button>
            </div>
        </div>
    </form>
    </div>
@endsection

@push('js')
    <script src="{{ asset('assets/admin/js/select2.min.js') }}"></script>
    <script>
        var radioCheckbox = 1;

        /**
         * add multiple field on click plus icon
         */
        $(document).on("click",".plus-icon", function (){
            var $this,type,appendHtml;
             $this = this;
            var counts;
            var countsMigration = parseInt($('.migration-wrapper-count').attr('attr-data-count'));
            var countsSeederFactory = parseInt($('.seeder-factory-wrapper-count').attr('attr-data-count'));
            var countsText = parseInt($('.text-wrapper-count').attr('attr-data-count'));

            type = $($this).attr('attr-data');
            if(type == 'migration' ){
                countsMigration += 1;
                counts = countsMigration;
            }
            if(type == 'seederFactory' ){
                countsSeederFactory += 1;
                counts = countsSeederFactory;
            }
            if(type == 'text' ){
                countsText += 1;
                counts = countsText;
            }
            appendHtml = '.'+type+'Append';
            $.ajax({
                url:'{{ route('admin.generator.render') }}',
                method: 'POST',
                dataType: 'json',
                data: { _token: '{{csrf_token()}}',type,counts},
                success: function (data) {

                    if(type == 'migration'){
                        $('.migration-wrapper-count').attr('attr-data-count',data.count);
                        $(appendHtml).append(data.data);
                        $('.select2Type').select2({
                            placeholder:"Select Type",
                            width: '90%',
                        });
                        $('.select2Modifier').select2({
                            placeholder:"Select Modifier",
                            width: '90%',
                        });
                    }
                    else if(type == 'text'){
                        $('.text-wrapper-count').attr('attr-data-count',data.count);
                        $(appendHtml).append(data.data);
                        $('.input_type').select2({
                            placeholder:"Select Input Type",
                            width: '19%',
                        });
                    }
                    else if(type == 'seederFactory'){
                        $('.seeder-factory-wrapper-count').attr('attr-data-count',data.count);
                        $(appendHtml).append(data.data);
                        $('.select2Type').select2({
                            placeholder:"Select Input Type",
                            width: '90%',
                        });
                    }
                    else{
                        $(appendHtml).append(data.data);
                    }
                }
            });
        });


        $(document).on("click",".plus-icon-sub", function (){
            var $this = $(this);
            counts = $this.parent('.radioCheckbox').attr('attr-num');
            $.ajax({
                url:'{{ route('admin.generator.sub-render') }}',
                method: 'POST',
                dataType: 'json',
                data: { _token: '{{csrf_token()}}',counts},
                success: function (data) {
                    $this.parent('.radioCheckbox').parent('.text-input-field-wrapper').children('.radioCheckboxAppend').append(data.data);
                }
            });
        });

        $(document).on("click",".plus-icon-img", function (){
            var $this = $(this);
            counts = $this.parent('.imageDimensions').attr('attr-num');
            $.ajax({
                url:'{{ route('admin.generator.image-render') }}',
                method: 'POST',
                dataType: 'json',
                data: { _token: '{{csrf_token()}}',counts},
                success: function (data) {
                    $this.parent('.imageDimensions').parent('.text-input-field-wrapper').children('.imageDimensionsAppend').append(data.data);
                }
            });
        });

        /**
         * remove sub row
         */
        $(document).on("click",".minus-icon-sub", function (){
            var $this,id,data;
            $this = this;
            id = $($this).attr('attr-data-sub-id');
            data = 'sub_wrapper_'+id;
            $(this).closest('#'+data).remove();
        });


        $(document).on("focusout",".input_field_name", function (){
            var $this;
            $this = $(this);
            // showTable
            var name = $this.val();
            // To change attr value
            $this.closest('.text-input-field-wrapper').find(".showTable").attr('value',name);
            // to append
            // $this.closest('.text-input-field-wrapper').find(".showTable").append(`<option selected hidden value="${name}">${name}</option>`);
        });


            /**
         * remove row
         */
        $(document).on("click",".minus-icon", function (){
            var $this,id,data;
            $this = this;
            id = $($this).attr('attr-data-id');
            data = 'wrapper_'+id;
            $(this).closest('#'+data).remove();
        });


        /**
         *  select 2
         */
        $(document).ready(function() {
            $('.select2Type').select2({
                placeholder:"Select Type, string",
                width: '90%',
            });
            $('.select2Modifier').select2({
                placeholder:"Select Modifier, default",
                width: '90%',
            });
            $('.input_type').select2({
                placeholder:"Select Input Type",
                width: '19%',
            });
        });

        $(document).on('change','.input_type', function (){
            var $this = $(this);
            if($this.val() == 'radio' || $this.val() == 'checkbox'){
                $this.parent('.all-input-wrapper').parent('.text-input-field-wrapper').children('.radioCheckbox').show();
                $this.parent('.all-input-wrapper').parent('.text-input-field-wrapper').children('.imageDimensions').hide();
            }
            else if($this.val() == 'file'){
                $this.parent('.all-input-wrapper').parent('.text-input-field-wrapper').children('.imageDimensions').show();
                $this.parent('.all-input-wrapper').parent('.text-input-field-wrapper').children('.radioCheckbox').hide();
            }
            else{
                $this.parent('.all-input-wrapper').parent('.text-input-field-wrapper').children('.radioCheckbox').hide();
                $this.parent('.all-input-wrapper').parent('.text-input-field-wrapper').children('.imageDimensions').hide();
            }
        });

        // $('.default_value').prop('disabled', true);
        // document.getElementsByClassName("default_value").disabled = true;
        // $(document).on('change', '.select2Modifier', function(){
        //     var $this = $(this);
        //     if($this.val() == 'default'){
        //         $this.closest('.text-input-field-wrapper').find('.default_value').prop('disabled', false);
        //     }
        //     else{
        //         $this.closest('.text-input-field-wrapper').find('.default_value').prop('disabled', true);
        //     }
        // });

        $(document).on('click','#migration', function (){
            if($(this).prop('checked') === true){
                $('.migrationSection').show();
            }else{
                $('.migrationSection').hide();
            }
        });

        $(document).on('click','#model', function (){
            if($(this).prop('checked') === true){
                $('.modalSection').show();
            }else{
                $('.modalSection').hide();
            }
        });
        $(document).on('click','#seederAndFactories', function (){
            if($(this).prop('checked') === true){
                $('.seederFactorySection').show();
            }else{
                $('.seederFactorySection').hide();
            }
        });
        $(document).on('click','#seederAndFactories', function (){
            if($(this).prop('checked') === true){
                $('.viewSection').show();
            }else{
                $('.viewSection').hide();
            }
        });

        $(document).on('focusout','.modelFillable', function (){
                var $this = $(this);
                var $value = $.trim($this.val());
                if($value.length !== 0){
                    var $arrayValue = $value.split(',');
                    $arrayValue= $arrayValue.filter(Boolean);
                    var $arrayValueCount = $arrayValue.length;

                    var type = ['migration','text','seederFactory'];
                    // var appendHtml = '.'+type+'Append';
                    var counts = $arrayValueCount;
                    $.ajax({
                        url:'{{ route('admin.generator.renderAll') }}',
                        method: 'POST',
                        dataType: 'json',
                        data: { _token: '{{csrf_token()}}',type,counts},
                        success: function (data) {
                            if(jQuery.inArray("migration", type) !== -1) {
                                // if(data.dataMigrationCount == 1){
                                    var ty = 'migration';
                                    var appendHtml = '.'+ty+'Append';
                                    $('#collapseMigrationSectionOne').attr('class','collapse show');
                                    $('.migration-wrapper-count').attr('attr-data-count',counts);
                                    $(appendHtml).empty();
                                    $(appendHtml).append(data.dataMigration);
                                    for (let i = 1; i <= $arrayValue.length; i++) {
                                        var cls = ty+i;
                                        $('.'+cls).val($arrayValue[i-1]);
                                    }
                                    $('.col-migration-section').attr('class','col-md-1 pull-right');
                                    $('.select2Type').select2({
                                        placeholder:"Select Type",
                                        width: '90%',
                                    });
                                    $('.select2Modifier').select2({
                                        placeholder:"Select Modifier",
                                        width: '90%',
                                    });
                                // }

                            }
                            if(jQuery.inArray("seederFactory", type)) {
                                // if(data.dataSeederFactoryCount == 1) {
                                    var ty = 'seederFactory';
                                    var appendHtml = '.'+ty+'Append';
                                    $('#collapseFactorySectionOne').attr('class','collapse show');
                                    $('.seeder-factory-wrapper-count').attr('attr-data-count',counts);
                                    $(appendHtml).empty();
                                    $(appendHtml).append(data.dataSeederFactory);
                                    for (let i = 1; i <= $arrayValue.length; i++) {
                                        var cls = ty+i;
                                        $('.'+cls).val($arrayValue[i-1]);
                                    }
                                    $('.col-migration-section').attr('class','col-md-1 pull-right');
                                    $('.select2Type').select2({
                                        placeholder:"Select Type",
                                        width: '90%',
                                    });
                                    $('.select2Modifier').select2({
                                        placeholder:"Select Modifier",
                                        width: '90%',
                                    });
                                // }
                            }

                            if(jQuery.inArray("text", type)) {
                                // if (data.dataTextCounts == 1) {
                                    var ty = 'text';
                                    var appendHtml = '.'+ty+'Append';
                                    $('#collapseOne').attr('class','collapse show');
                                    $('.text-wrapper-count').attr('attr-data-count', counts);
                                    $(appendHtml).empty();
                                    $(appendHtml).append(data.dataText);
                                    for (let i = 1; i <= $arrayValue.length; i++) {
                                        var cls = ty + i;
                                        var clsLabel = ty+'Label'+ i;
                                        var labelText = $arrayValue[i - 1];
                                        var finalLabelText = labelText.substr(0,1).toUpperCase()+labelText.substr(1);
                                        $('.' + cls).val(labelText);
                                        $('.' + clsLabel).val(finalLabelText);
                                    }
                                    $('.col-migration-section').attr('class', 'col-md-1 pull-right');
                                    $('.select2Type').select2({
                                        placeholder: "Select Type",
                                        width: '90%',
                                    });
                                    $('.select2Modifier').select2({
                                        placeholder: "Select Modifier",
                                        width: '90%',
                                    });
                                // }
                            }

                        }
                    });
                }
        });
    </script>

@endpush

