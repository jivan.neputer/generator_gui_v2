<div class="migrationSection">
<h4>This is Migration Section</h4>
<div id="accordion">
    <div class="card">
        <div class="card-header" id="headingMigrationSectionOne">
            <h5 class="mb-0">Migration Section <code>Check your migration file, After migration file created!!!</code>
                <span data-toggle="collapse" data-target="#collapseMigrationSectionOne" aria-expanded="true" aria-controls="collapseMigrationSectionOne" class="col-md-1 pull-right col-migration-section">Click Here</span>
            </h5>
        </div>

        <div id="collapseMigrationSectionOne" class="collapse" aria-labelledby="headingMigrationSectionOne" data-parent="#accordion">
            <div class="card-body">
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-12" for="text_input">Text Field
                        <span class="col-md-2 pull-right plus-icon" attr-data="migration"><i class="btn btn-info glyphicon glyphicon-plus-sign pull-right"></i></span>
                    </label>
                    <div class="text-input-field-wrapper migration-wrapper-count form-inline" attr-data-count="1" style="margin-bottom: 8px;">
                        <div class="form-group form-inline col-lg-6">
                            <div class="col-lg-6">
                                <label class="control-label" for=""><b>Column Name:</b></label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" style="width: 198px;" class="form-control migration1" name="migration[column1][]" placeholder="my_name,name" required>
                            </div>
                        </div>
                        <div class="form-group form-inline col-lg-6">
                            <div class="col-lg-6">
                                <label class="control-label" for=""><b>Column Type:</b></label>
                            </div>
                            <div class="col-lg-6">
                                <select name="migration[column1][]" id="" class="form-control select2Type" required>
                                    @include('admin.generator-dashboard.form.common.column-type')
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-inline col-lg-6">
                            <div class="col-lg-6">
                                <label class="control-label" for=""><b>Column Modifier:</b></label>
                            </div>
                            <div class="col-lg-6">
                                <select name="migration[column1][]" id="" class="form-control select2Modifier">
                                    @include('admin.generator-dashboard.form.common.column-modifier')
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-inline col-lg-6">
                            <div class="col-lg-6">
                                <label class="control-label" for=""><b>Default Value:</b></label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" style="width: 196px;" class="form-control default_value" name="migration[column1][]" placeholder="Default Value">
                            </div>
                        </div>
                        <div class="form-group form-inline col-lg-6">
                            <div class="col-lg-6">
                                <label class="control-label" for=""><b>Comment :</b></label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" style="width: 200px;" class="form-control" name="migration[column1][]" placeholder="Comment">
                            </div>
                        </div>
                    </div>
                    <span class="migrationAppend"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
</div>
