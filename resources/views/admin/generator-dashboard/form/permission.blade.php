<h4>This is Permission Section</h4>
<div id="accordion">
    <div class="card">
        <div class="card-header" id="headingPermissionOne">
            <h5 class="mb-0">Permission
            <span data-toggle="collapse" data-target="#collapsePermissionOne" aria-expanded="true" aria-controls="collapsePermissionOne" class="col-md-1 pull-right">Click Here</span>
            </h5>
        </div>

        <div id="collapsePermissionOne" class="collapse" aria-labelledby="headingPermissionOne" data-parent="#accordion">
            <div class="card-body">

                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <div class="text-input-field-wrapper">
                        <div class="form-group form-inline permission ">
                            <div class="form-inline col-lg-4">
                                <div class="col-lg-8">
                                    <label class="control-label" for="migration">Migration :</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="checkbox" id="migration" checked="checked" name="permission[migration-permission]" value="true" >
                                </div>
                            </div>
                            <div class="form-inline col-lg-4">
                                <div class="col-lg-8">
                                    <label class="control-label" for="model">Model :</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="checkbox" id="model" checked="checked" name="permission[model-permission]" value="true" >
                                </div>
                            </div>
                            <div class="form-inline col-lg-4">
                                <div class="col-lg-8">
                                    <label class="control-label" for="request">Request :</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="checkbox" id="request" checked="checked" name="permission[request-permission]" value="true" >
                                </div>
                            </div>
                            <div class="form-inline col-lg-4">
                                <div class="col-lg-8">
                                    <label class="control-label" for="seederAndFactories">Seeder And Factories :</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="checkbox" id="seederAndFactories" checked="checked" name="permission[seederAndFactories-permission]" value="true" >
                                </div>
                            </div>
                            <div class="form-inline col-lg-4">
                                <div class="col-lg-8">
                                    <label class="control-label" for="routes">Routes :</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="checkbox" id="routes" checked="checked" name="permission[routes-permission]" value="true" >
                                </div>
                            </div>
                            <div class="form-inline col-lg-4">
                                <div class="col-lg-8">
                                    <label class="control-label" for="service">Service :</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="checkbox" id="service" checked="checked" name="permission[service-permission]" value="true" >
                                </div>
                            </div>
                            <div class="form-inline col-lg-4">
                                <div class="col-lg-8">
                                    <label class="control-label" for="controller">Controller :</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="checkbox" id="controller" checked="checked" name="permission[controller-permission]" value="true" >
                                </div>
                            </div>
                            <div class="form-inline col-lg-4">
                                <div class="col-lg-8">
                                    <label class="control-label" for="view">View :</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="checkbox" id="view" checked="checked" name="permission[view-permission]" value="true" >
                                </div>
                            </div>
                            <div class="form-inline col-lg-4">
                                <div class="col-lg-8">
                                    <label class="control-label" for="appendMenu">Append Menu :</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="checkbox" id="appendMenu" checked="checked" name="permission[appendMenu-permission]" value="true" >
                                </div>
                            </div>
                            <div class="form-inline col-lg-4">
                                <div class="col-lg-8">
                                    <label class="control-label" for="appendSeeder">Append Seeder :</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="checkbox" id="appendSeeder" checked="checked" name="permission[appendSeeder-permission]" value="true" >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
