<div class="modalSection">
<h4>This is model column name Section</h4>
<div id="accordion">
    <div class="card">
        <div class="card-header" id="headingModelColumnNameOne">
            <h5 class="mb-0">Model Fillable
                <span data-toggle="collapse" data-target="#collapseModelColumnNameOne" aria-expanded="true" aria-controls="collapseModelColumnNameOne" class="col-md-1 pull-right">Click Here</span>
            </h5>
        </div>

        <div id="collapseModelColumnNameOne" class="collapse" aria-labelledby="headingModelColumnNameOne" data-parent="#accordion">
            <div class="card-body">

                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <div class="text-input-field-wrapper">
                        <div class="form-group form-inline">
                            <label class="control-label col-md-2" for=""><b>Fillable Column:</b></label>
                            <textarea class="form-control col-md-10 modelFillable" name="modelFillable" placeholder="Enter Model Column Name! Ex:- category,slug,product,file_name" cols="90" rows="5" required></textarea>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<br/>
</div>
