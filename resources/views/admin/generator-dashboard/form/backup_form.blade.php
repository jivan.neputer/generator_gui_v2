<h4>This is View Section</h4>
<div id="accordion">
    <div class="card">
        <div class="card-header" id="headingOne">
            <h5 class="mb-0">Form Section
                <span data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="col-md-1 pull-right">Click Here</span>
            </h5>
        </div>

        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body">
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-12" for="text_input">Text Field
                        <span class="col-md-2 pull-right plus-icon" attr-data="text"><i class="btn btn-info glyphicon glyphicon-plus-sign pull-right"></i></span>
                    </label>
                    <div class="text-input-field-wrapper">
                        <div class="form-group form-inline">
                            <label class="control-label col-md-2" for=""><b>Display Name:</b></label>
                            <input type="text" class="form-control col-md-10" name="text[text_display_name][]" placeholder="your Text Input Display Name! ex: First Name">
                        </div>
                        <div class="form-group form-inline">
                            <label class="control-label col-md-2" for=""><b>Input Field Name:</b></label>
                            <input type="text" class="form-control col-md-10" name="text[text_input_name][]" placeholder="your Input Field Name! ex: first_name">
                        </div>
                        <div class="form-group form-inline">
                            <label class="control-label col-md-2" for=""><b>Placeholder:</b></label>
                            <input type="text" class="form-control col-md-10" name="text[text_placeholder][]" placeholder="your Input placeholder! ex: Plz Enter Your First Name">
                        </div>
                    </div>

                    <span class="textAppend"></span>
                </div>
                <hr>
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-12" for="text_input">Email Field (make it here clickable)
                        <span class="col-md-2 pull-right plus-icon" attr-data="email"><i class="btn btn-info glyphicon glyphicon-plus-sign pull-right"></i></span>
                    </label>
                    <div class="text-input-field-wrapper">
                        <div class="form-group form-inline">
                            <label class="control-label col-md-2" for=""><b>Display Name:</b></label>
                            <input type="text" class="form-control col-md-10" name="email[email_display_name][]" placeholder="your Text Input Display Name! ex: Email">
                        </div>
                        <div class="form-group form-inline">
                            <label class="control-label col-md-2" for=""><b>Input Field Name:</b></label>
                            <input type="text" class="form-control col-md-10" name="email[email_input_name][]" placeholder="your Input Field Name! ex: email">
                        </div>
                        <div class="form-group form-inline">
                            <label class="control-label col-md-2" for=""><b>Placeholder:</b></label>
                            <input type="text" class="form-control col-md-10" name="email[email_placeholder][]" placeholder="your Input placeholder! ex: Plz Enter Your Email">
                        </div>
                    </div>
                    <span class="emailAppend"></span>
                </div>
                <hr>

            </div>
        </div>
    </div>


    <div class="card">
        <div class="card-header" id="headingTwo">
            <h5 class="mb-0">Email Input Field
                <span data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" class="col-md-1 pull-right">Click Here</span>
            </h5>
        </div>

        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
            <div class="card-body">

                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-12" for="text_input">Email Field (make it here clickable)
                        <span class="col-md-2 pull-right plus-icon" attr-data="email"><i class="btn btn-info glyphicon glyphicon-plus-sign pull-right"></i></span>
                    </label>
                    <div class="text-input-field-wrapper">
                        <div class="form-group form-inline">
                            <label class="control-label col-md-2" for=""><b>Display Name:</b></label>
                            <input type="text" class="form-control col-md-10" name="email[email_display_name][]" placeholder="your Text Input Display Name! ex: Email">
                        </div>
                        <div class="form-group form-inline">
                            <label class="control-label col-md-2" for=""><b>Input Field Name:</b></label>
                            <input type="text" class="form-control col-md-10" name="email[email_input_name][]" placeholder="your Input Field Name! ex: email">
                        </div>
                        <div class="form-group form-inline">
                            <label class="control-label col-md-2" for=""><b>Placeholder:</b></label>
                            <input type="text" class="form-control col-md-10" name="email[email_placeholder][]" placeholder="your Input placeholder! ex: Plz Enter Your Email">
                        </div>
                    </div>
                    <span class="emailAppend"></span>
                </div>

            </div>
        </div>
    </div>
</div>
<br/>






