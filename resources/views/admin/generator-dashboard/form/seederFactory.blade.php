<div class="seederFactorySection">
<h4>This is Seeder Section</h4>
<div id="accordion">
    <div class="card">
        <div class="card-header" id="headingMigrationSectionOne">
            <h5 class="mb-0">Seeder and Factory Section <code>Check your Factory file, After factory file created!!!</code>
                <span data-toggle="collapse" data-target="#collapseFactorySectionOne" aria-expanded="true" aria-controls="collapseFactorySectionOne" class="col-md-1 pull-right">Click Here</span>
            </h5>
        </div>

        <div id="collapseFactorySectionOne" class="collapse" aria-labelledby="collapseFactorySectionOne" data-parent="#accordion">
            <div class="card-body">
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-12" for="text_input">Text Field
                        <span class="col-md-2 pull-right plus-icon" attr-data="seederFactory"><i class="btn btn-info glyphicon glyphicon-plus-sign pull-right"></i></span>
                    </label>
                    <div class="text-input-field-wrapper form-inline seeder-factory-wrapper-count" attr-data-count="1" style="margin-bottom: 8px;">
                        <div class="form-group form-inline col-lg-6">
                            <div class="col-lg-6">
                                <label class="control-label" for=""><b>Column Name:</b></label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" style="width: 198px;" class="form-control seederFactory1" name="seederFactory[column1][]" placeholder="my_name,name" required>
                            </div>
                        </div>
                        <div class="form-group form-inline col-lg-6">
                            <div class="col-lg-6">
                                <label class="control-label" for=""><b>Faker Type:</b></label>
                            </div>
                            <div class="col-lg-6">
                                <select name="seederFactory[column1][]" id="" class="form-control select2Type" required>
                                    @include('admin.generator-dashboard.form.common.factory-column-type')
                                </select>
                            </div>
                        </div>
                    </div>
                    <span class="seederFactoryAppend"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
</div>
