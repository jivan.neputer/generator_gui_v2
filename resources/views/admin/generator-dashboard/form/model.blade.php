<h4>This is Form Section</h4>
<div id="accordion">
    <div class="card">
        <div class="card-header" id="headingModelNameOne">
            <h5 class="mb-0">Model Section
                <span data-toggle="collapse" data-target="#collapseModelNameOne" aria-expanded="true" aria-controls="collapseModelNameOne" class="col-md-1 pull-right">Click Here</span>
            </h5>
        </div>

        <div id="collapseModelNameOne" class="collapse show" aria-labelledby="headingModelNameOne" data-parent="#accordion">
            <div class="card-body">

                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <div class="text-input-field-wrapper">
                        <div class="form-group form-inline">
                            <label class="control-label col-md-2" for=""><b>Model Name:</b></label>
                            <input type="text" class="form-control col-md-10" name="modelName" placeholder="Enter Model Name! Ex:- category or menu-category" required>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<br/>






