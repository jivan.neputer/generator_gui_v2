<div class="text-input-field-wrapper">
    <div class="form-group form-inline">
        <label class="control-label col-md-2" for=""><b>Display Name:</b></label>
        <input type="text" class="form-control col-md-10" name="email[email_display_name][]" placeholder="your Text Input Display Name! ex: Email">
    </div>
    <div class="form-group form-inline">
        <label class="control-label col-md-2" for=""><b>Input Field Name:</b></label>
        <input type="text" class="form-control col-md-10" name="email[email_input_name][]" placeholder="your Input Field Name! ex: email">
    </div>
    <div class="form-group form-inline">
        <label class="control-label col-md-2" for=""><b>Placeholder:</b></label>
        <input type="text" class="form-control col-md-10" name="email[email_placeholder][]" placeholder="your Input placeholder! ex: Plz Enter Your Email">
    </div>
</div>
