<div class="form-group form-inline imageDimensions row col-12 d-flex" id="sub_wrapper_{{$counts}}">
    <div class="image_dimensions_width form-inline  col-4">
        <label class="control-label col-md-6 " for=""><b>Width</b></label>
        <input type="text" class="form-control col-md-6 " name="text[dimensions][image_dimensions_width][{{$counts}}][]">
    </div>
    <div class="image_dimensions_height  form-inline col-4">
        <label class="control-label col-md-7" for=""><b>Height:</b></label>
        <input type="text" class="form-control col-md-5" name="text[dimensions][image_dimensions_height][{{$counts}}][]">
    </div>
    <div class="image_dimensions_quality form-inline  col-4">
        <label class="control-label col-md-6" for=""><b> Quality:</b></label>
        <input type="text" class="form-control col-md-6" name="text[dimensions][image_dimensions_quality][{{$counts}}][]">
    </div>
    <span class="minus-icon-sub pull-right-sub" attr-data-sub-id = "{{$counts}}"><i class="btn btn-info glyphicon glyphicon-minus-sign pull-right"></i></span>
</div>
