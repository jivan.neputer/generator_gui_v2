<div class="form-group form-inline radioCheckbox row col-12 d-flex" id="sub_wrapper_{{$counts}}">
    <div class="checkbox_radio_label form-inline checkbox_radio_common col-4">
        <label class="control-label col-md-6 " for=""><b>Label Name:</b></label>
        <input type="text" class="form-control col-md-6 " name="text[checkbox_radio_label][{{$counts}}][]">
    </div>
    <div class="checkbox_radio_id checkbox_radio_common form-inline col-4">
        <label class="control-label col-md-7" for=""><b>Input ID(label for):</b></label>
        <input type="text" class="form-control col-md-5" name="text[checkbox_radio_id][{{$counts}}][]">
    </div>
    <div class="checkbox_radio_default_value form-inline checkbox_radio_common col-4">
        <label class="control-label col-md-6" for=""><b> Value:</b></label>
        <input type="text" class="form-control col-md-6" name="text[checkbox_radio_default_value][{{$counts}}][]">
    </div>
    <span class="minus-icon-sub pull-right-sub" attr-data-sub-id = "{{$counts}}"><i class="btn btn-info glyphicon glyphicon-minus-sign pull-right"></i></span>
</div>
