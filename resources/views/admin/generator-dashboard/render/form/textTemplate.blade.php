<div class="text-input-field-wrapper" id="wrapper_{{$counts}}">
    <div class="form-group col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 35px;">
        <span class="pull-right minus-icon" attr-data="text" attr-data-id="{{$counts}}">
            <i class="btn btn-info glyphicon glyphicon-minus-sign pull-right"></i>
        </span>
    </div>
    <div class="form-group form-inline">
        <label class="control-label col-md-2" for=""><b>Display Name:</b></label>
        <input type="text" class="form-control col-md-10 textLabel{{$counts}}" name="text[text_display_name][]" placeholder="your Text Input Display Name! ex: First Name" required>
    </div>
    <div class="form-group form-inline">
        <label class="control-label col-md-2" for=""><b>Input Field Name:</b></label>
        <input type="text" class="form-control col-md-10 input_field_name text{{$counts}}" name="text[text_input_name][]" placeholder="your Input Field Name! ex: first_name" required>
    </div>
    <div class="form-group form-inline">
        <label class="control-label col-md-2" for=""><b>Placeholder:</b></label>
        <input type="text" class="form-control col-md-10" name="text[text_placeholder][]" placeholder="your Input placeholder! ex: Plz Enter Your First Name">
    </div>
    <div class="form-group form-inline">
        <label class="control-label col-md-2" for=""><b>Show Table:</b></label>
        <select name="text[show_table][{{$counts}}][]" class="form-control show_table col-md-10" multiple>
            <option selected class="showTable" value="null">None </option>
            <option value="s_show">Show</option>
            <option value="t_table">Table</option>
        </select>
    </div>
    <div class="form-group form-inline all-input-wrapper">
        <label class="control-label col-md-2" for=""><b>Input Type:</b></label>
        <select name="text[input_type][]" class="form-control input_type">
            <option value="text">Text</option>
            <option value="checkbox">Checkbox</option>
            <option value="textarea">TextArea</option>
            <option value="email">Email</option>
            <option value="date">Date</option>
            <option value="file">File</option>
            <option value="month">Month</option>
            <option value="number">Number</option>
            <option value="password">Password</option>
            <option value="radio">Radio</option>
            <option value="tel">Tel</option>
            <option value="time">Time</option>
            <option value="url">URL</option>
        </select>
    </div>
    <div class="form-group form-inline radioCheckbox row col-12 " attr-num = "{{ $counts }}" style="display: none;">
        <div class="checkbox_radio_label form-inline checkbox_radio_common col-4">
            <label class="control-label col-md-6 " for=""><b>Label Name:</b></label>
            <input type="text" class="form-control col-md-6 " name="text[checkbox_radio_label][{{$counts}}][]">
        </div>
        <div class="checkbox_radio_id checkbox_radio_common form-inline col-4">
            <label class="control-label col-md-7" for=""><b>Input ID(label for):</b></label>
            <input type="text" class="form-control col-md-5" name="text[checkbox_radio_id][{{$counts}}][]">
        </div>
        <div class="checkbox_radio_default_value form-inline checkbox_radio_common col-4">
            <label class="control-label col-md-6" for=""><b> Value:</b></label>
            <input type="text" class="form-control col-md-6" name="text[checkbox_radio_default_value][{{$counts}}][]">
        </div>
        <span class="plus-icon-sub pull-right-sub" ><i class="btn btn-info glyphicon glyphicon-plus-sign pull-right"></i></span>
    </div>
    <div class="form-group form-inline imageDimensions row col-12 " style="display: none;" attr-num = "{{$counts}}">
        <div class="image_dimensions_width form-inline col-4">
            <label class="control-label col-md-6 " for=""><b>Width</b></label>
            <input type="text" class="form-control col-md-6 " name="text[dimensions][image_dimensions_width][{{$counts}}][]">
        </div>
        <div class="image_dimensions_height form-inline col-4">
            <label class="control-label col-md-7" for=""><b>Height:</b></label>
            <input type="text" class="form-control col-md-5" name="text[dimensions][image_dimensions_height][{{$counts}}][]">
        </div>
        <div class="image_dimensions_quality form-inline  col-4">
            <label class="control-label col-md-6" for=""><b> Quality:</b></label>
            <input type="text" class="form-control col-md-6" name="text[dimensions][image_dimensions_quality][{{$counts}}][]">
        </div>
        <span class="plus-icon-img pull-right-sub" ><i class="btn btn-info glyphicon glyphicon-plus-sign pull-right"></i></span>
    </div>
    <span class="radioCheckboxAppend"></span>
    <span class="imageDimensionsAppend"></span>
</div>
