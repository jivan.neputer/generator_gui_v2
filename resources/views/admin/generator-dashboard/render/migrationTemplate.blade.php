{{--<div class="text-input-field-wrapper form-inline custom_width_wrapper">--}}
{{--    <div class="form-group form-inline custom_width25">--}}
{{--        <label class="control-label" for=""><b>Column Name:</b></label>--}}
{{--        <input type="text" class="form-control" name="migration[column{{$counts}}][]" placeholder="my_name,name">--}}
{{--    </div>--}}
{{--    <div class="form-group form-inline custom_width25">--}}
{{--        <label class="control-label" for=""><b>Column Type:</b></label>--}}
{{--        <select name="migration[column{{$counts}}][]" id="" class="form-control select2Type">--}}
{{--            @include('admin.generator-dashboard.form.common.column-type')--}}
{{--        </select>--}}
{{--    </div>--}}
{{--    <div class="form-group form-inline custom_width25">--}}
{{--        <label class="control-label" for=""><b>Column Modifier:</b></label>--}}
{{--        <select name="migration[column{{$counts}}][]" id="" class="form-control select2Modifier">--}}
{{--            @include('admin.generator-dashboard.form.common.column-modifier')--}}
{{--        </select>--}}
{{--    </div>--}}
{{--    <div class="form-group form-inline custom_width25">--}}
{{--        <label class="control-label" for=""><b>Default Value:</b></label>--}}
{{--        <input type="text" class="form-control" name="migration[column{{$counts}}][]" placeholder="Default Value">--}}
{{--    </div>--}}
{{--</div>--}}
<div class="form-group" id ="wrapper_{{$counts}}">
    <div class="form-group col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 35px;">
        <span class="pull-right minus-icon" attr-data="migration" attr-data-id="{{$counts}}">
            <i class="btn btn-info glyphicon glyphicon-minus-sign pull-right"></i>
        </span>
    </div>
    <div class="text-input-field-wrapper form-inline ">
        <div class="form-group form-inline col-lg-6">
            <div class="col-lg-6">
                <label class="control-label" for=""><b>Column Name:</b></label>
            </div>
            <div class="col-lg-6">
                <input type="text" style="width: 198px;" class="form-control migration{{$counts}}" name="migration[column{{$counts}}][]" placeholder="my_name,name" required>
            </div>
        </div>
        <div class="form-group form-inline col-lg-6">
            <div class="col-lg-6">
                <label class="control-label" for=""><b>Column Type:</b></label>
            </div>
            <div class="col-lg-6">
                <select name="migration[column{{$counts}}][]" id="" class="form-control select2Type" required>
                    @include('admin.generator-dashboard.form.common.column-type')
                </select>
            </div>
        </div>
        <div class="form-group form-inline col-lg-6">
            <div class="col-lg-6">
                <label class="control-label" for=""><b>Column Modifier:</b></label>
            </div>
            <div class="col-lg-6">
                <select name="migration[column{{$counts}}][]" id="" class="form-control select2Modifier">
                    @include('admin.generator-dashboard.form.common.column-modifier')
                </select>
            </div>
        </div>
        <div class="form-group form-inline col-lg-6">
            <div class="col-lg-6">
                <label class="control-label" for=""><b>Default Value:</b></label>
            </div>
            <div class="col-lg-6">
                <input type="text" style="width: 196px;" class="form-control default_value" name="migration[column{{$counts}}][]" placeholder="Default Value">
            </div>
        </div>
        <div class="form-group form-inline col-lg-6">
            <div class="col-lg-6">
                <label class="control-label" for=""><b>Comment :</b></label>
            </div>
            <div class="col-lg-6">
                <input type="text" style="width: 200px;" class="form-control" name="migration[column{{$counts}}][]" placeholder="Default Value">
            </div>
        </div>
    </div>
</div>


