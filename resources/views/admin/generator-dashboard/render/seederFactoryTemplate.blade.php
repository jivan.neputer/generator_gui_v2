<div class="form-group" id ="wrapper_{{$counts}}">
    <div class="form-group col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 35px;">
        <span class="pull-right minus-icon" attr-data="seederFactory" attr-data-id="{{$counts}}">
            <i class="btn btn-info glyphicon glyphicon-minus-sign pull-right"></i>
        </span>
    </div>
    <div class="text-input-field-wrapper form-inline " style="margin-bottom: 8px;">
        <div class="form-group form-inline col-lg-6">
            <div class="col-lg-6">
                <label class="control-label" for=""><b>Column Name:</b></label>
            </div>
            <div class="col-lg-6">
                <input type="text" style="width: 198px;" class="form-control seederFactory{{$counts}}" name="seederFactory[column{{$counts}}][]" placeholder="my_name,name" required>
            </div>
        </div>
        <div class="form-group form-inline col-lg-6">
            <div class="col-lg-6">
                <label class="control-label" for=""><b>Faker Type:</b></label>
            </div>
            <div class="col-lg-6">
                <select name="seederFactory[column{{$counts}}][]" id="" class="form-control select2Type" required>
                    @include('admin.generator-dashboard.form.common.factory-column-type')
                </select>
            </div>
        </div>
    </div>
</div>
