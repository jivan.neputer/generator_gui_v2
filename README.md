# Laravel Service Pattern 
    Laravel service pattern with inspinia theme

### Framework
    1. Laravel 6
    2. With auth
    3. No NPM Use
        
### List of Package Use
    1. laravel collective form
    2. yajra datatable

### List of Task
    1. Inspinia login form implemented(with loging authentication).
    2. And one simple category crud
    3. Listing data using yajra datatable
    4. Data operation using laravel collection form
    5. Just simple Crud no image operation use
    6. sweet alert for delete operation.
 

### Implementing inspinia login
    1. Just copy and paste admin folder inside your projecct
        Example:- resources/views/admin copy 
                  this folder and paste into your project as same directive
    2. Just copy and paste admin folder inside your project
        Example:- public/assets/admin copy
            this folder and paste into your project as same directive.
            
   
### In Route
    In web.php
        copy this line of code inside your web.php
        Route::group(['prefix' => 'admin/', 'as' => 'admin.', 'namespace' => 'Admin\\', 'middleware' => 'auth'], function() {
            /*
                this is admin dashboard
             */
            Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
        });  
   
   
### Now Manage Your Auth Route:
            1. In loginController.php(app/http/controller/auth/loginController)
            paste this line of code for custom design login
            
                /**
                 * Show the application's login form.
                 *
                 * @return \Illuminate\Http\Response
                 */
                public function showLoginForm()
                {
                    return view('admin.auth.login');
                }
            
                /**
                 * Log the user out of the application.
                 *
                 * @param  \Illuminate\Http\Request  $request
                 * @return \Illuminate\Http\Response
                 */
                public function logout(Request $request)
                {
                    $this->guard()->logout();
            
                    $request->session()->invalidate();
            
                    $request->session()->regenerateToken();
            
                    return $this->loggedOut($request) ?: redirect('/login');
                }
                
            2. In RegisterController
                /**
                     * Show the application registration form.
                     *
                     * @return \Illuminate\Http\Response
                     */
                    public function showRegistrationForm()
                    {
                        return view('admin.auth.register');
                    }
                
                    /**
                     * Create a new user instance after a valid registration.
                     *
                     * @param  array  $data
                     * @return \App\User
                     */
                    protected function create(array $data)
                    {
                        return User::create([
                            'name' => $data['name'],
                            'email' => $data['email'],
                            'password' => Hash::make($data['password']),
                        ]);
                    }
