<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin/auth/login');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin/', 'as' => 'admin.', 'namespace' => 'Admin\\', 'middleware' => 'auth'], function() {

    /**
     * Generator Route
     */
    Route::get('generator',  ['as' => 'generator',  'uses' => 'Generator\GeneratorDashboardController@index']);
    Route::post('generator/render',  ['as' => 'generator.render',  'uses' => 'Generator\GeneratorDashboardController@ajaxRender']);
    Route::post('generator/renderAll',  ['as' => 'generator.renderAll',  'uses' => 'Generator\GeneratorDashboardController@ajaxRenderAll']);
    Route::post('generator/sub-render',  ['as' => 'generator.sub-render',  'uses' => 'Generator\GeneratorDashboardController@ajaxSubRender']);
    Route::post('generator/image-render',  ['as' => 'generator.image-render',  'uses' => 'Generator\GeneratorDashboardController@ajaxImageRender']);
    Route::post('generator/create',  ['as' => 'generator.create',  'uses' => 'Generator\GeneratorController@create']);

    /**
     * Admin Dashboard Route
     */
    Route::get('dashboard',  ['as' => 'dashboard',  'uses' => 'DashboardController@index']);

    include_once(base_path('routes/admin.php'));

});
